package hu.unideb.inf.szakdolgozat.model.response;

import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;

import java.util.List;

/**
 * Created by zolikapi on 2017. 05. 02..
 */
public class TeacherDashboardResponse {
    List<LecturePersistence> lectures;

    public TeacherDashboardResponse() {
    }

    public TeacherDashboardResponse(List<LecturePersistence> lectures) {
        this.lectures = lectures;
    }

    public List<LecturePersistence> getLectures() {
        return lectures;
    }

    public void setLectures(List<LecturePersistence> lectures) {
        this.lectures = lectures;
    }
}
