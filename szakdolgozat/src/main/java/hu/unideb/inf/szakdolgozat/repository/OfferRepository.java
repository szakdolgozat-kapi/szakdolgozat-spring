package hu.unideb.inf.szakdolgozat.repository;

import hu.unideb.inf.szakdolgozat.model.persistence.OfferPersistence;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zolikapi on 2017. 04. 16..
 */
public interface OfferRepository extends JpaRepository<OfferPersistence, Integer> {
    List<OfferPersistence> findByIdentifier(long identifier);
    List<OfferPersistence> findByEventId(long eventId);
}
