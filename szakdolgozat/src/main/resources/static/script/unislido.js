/**
 * Created by zolikapi on 2017. 12. 26..
 */

function sendCodeSuccess(text){
    document.getElementById("welcomeBox").style.display = 'none';
    
    var questionText = document.getElementById("questionText");
    questionText.innerHTML = text;
    
    document.getElementById("inputBox").style.display = 'none';
    
    var questionBox = document.getElementById("questionBox");
    questionBox.style.display = 'block';
    questionBox.className = 'success';
}

function sendCodeError(){
    document.getElementById("welcomeBox").style.display = 'none';
    
    var questionText = document.getElementById("questionText");
    questionText.innerHTML = "Nincs megjeleníthető kérdés";
    
    document.getElementById("inputBox").style.display = 'none';
    
    var questionBox = document.getElementById("questionBox");
    questionBox.style.display = 'block';
    questionBox.className = 'error';
}

function sendCode() {
    
    var codeText = document.getElementById("code").value;
    sendRequest(codeText, sendCodeSuccess, sendCodeError);
    
    setInterval(function() {
        sendRequest(codeText, sendCodeSuccess, sendCodeError);
    }, 5000);
}

function sendRequest(codeText, successCallback, errorCallback){
    var xobj = new XMLHttpRequest();
    xobj.open('GET', "http://localhost:8080/lecture/actual?lectureId="+codeText, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200" && xobj.responseText) {
            successCallback(xobj.responseText);
        } else{
            errorCallback();
        }
    };
    xobj.send(null);
}

