/**
 * Created by zolikapi on 2017. 04. 14..
 */

var allLecture = null;

$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/candidataLectures?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var lectures = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });
                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
    
});

function reloadData(lectures) {
    allLecture = lectures;
    
    //delete all rows from table
    var table = $('#datatable-checkbox').DataTable();
    table
        .clear()
        .draw();

    for (id in lectures) {
        var lecture = lectures[id];
        addRow(lecture);
    }
}

function addRow(lecture){
    var t = $('#datatable-checkbox').DataTable();
    var input = '<div><input type="checkbox" class="flat-checkbox" name="table_records"'
        + 'id="'
        + lecture.identifier
        + '"><label for="' + lecture.identifier + '"></label></div>';
    
    t.row.add( [
            input,
            lecture.name,
            lecture.info,
            lecture.eventName,
            lecture.placeName,
            timeConverter(lecture.startTimeStamp),
            timeConverter(lecture.endTimeStamp)
        ] ).draw( false );

}

function getSelectedRows(){
    var selectedIds = new Array();
    var table = $('#datatable-checkbox').DataTable();
    $("input:checked", table.data().rows().nodes()).each(function(idx, th){
        selectedIds.push(th.id);
    });
    return selectedIds;
}

function accept() {
    var params= {loginToken: localStorage.login_token, identifierList : getSelectedRows()};
    
    console.log(params);

    NProgress.start();
    $.post('/api/v1/acceptCandidataLectures', params,
        function(response){
            NProgress.done();
            console.log(response);
            if (response.status.status === 200){

                console.log(response);

                var lectures = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });
                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
}

function decline() {
    var params= {loginToken: localStorage.login_token, identifierList : getSelectedRows()};

    NProgress.start();
    $.post('/api/v1/declineCandidataLectures', params,
        function(response){
            NProgress.done();
            console.log(response);
            if (response.status.status === 200){

                console.log(response);

                var lectures = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });
                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
}
