/**
 * Created by zolikapi on 2017. 04. 11..
 */

/* Adding the script tag to the head as suggested before */

var head = document.getElementsByTagName('head')[0];
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = "http://code.jquery.com/jquery-2.2.1.min.js";

// Then bind the event to the callback function.
// There are several events for cross browser compatibility.
script.onreadystatechange = handler;
script.onload = handler;

// Fire the loading
head.appendChild(script);

var lectureId = null;
var actualQuestion = null;

function handler(){
    getActualQuestion();
    
    setInterval(function() {
        getActualQuestion();
    }, 5000);
}

function getActualQuestion(){
    if (typeof(lectureId) === 'undefined'){
        lectureId = getCookie('trackingLectureId');
    }
    
    $.get('/api/v1/lecture/actual?lectureId='+lectureId, 
         function(response){
            var questionText = document.getElementById("questionText");
        
            if (response.status.status === 200){
                questionText.className = 'success';
                questionText.innerHTML = response.data.question.questionString;
                
                if (actualQuestion != response.data.question.id){
                    actualQuestion = response.data.question.id;
                    refreshTable();
                }
            }else{
                questionText.className = 'error';
                questionText.innerHTML = response.status.messageHu;
            }
        });
}

function refreshTable(){
    
    $.get('/api/v1/lecture/allAcceptedQuestion?lectureId='+lectureId, 
         function(response){
            if (response.status.status === 200){
                console.log(response);
                
                //remove all row
                $("#questionTable td").remove();
                //add rows
                response.data.arrayList.forEach(function(question) {
                    var questionText = question.question;
                    var dateString = timeConverter(question.acceptedDate)
                    addLineToTable(questionText, dateString);
                });
                
            }else{
                console.log(response);
            }
        }, 'json');   
}

function addLineToTable(question, dateTime){
    var table = document.getElementById("questionTable");
    var cellCount = table.rows.length;
    var row = table.insertRow(cellCount);
    var cell0 = row.insertCell(0)
    var cell1 = row.insertCell(1);
    var cell2 = row.insertCell(2);

    cell0.innerHTML = cellCount;
    cell1.innerHTML = question;
    cell2.innerHTML = dateTime;
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var monthNames = ["Január", "Február", "Március", "Április", "Május", "Június",
                    "Július", "Augusztus", "Szeptember", "Október", "November", "December"
                   ];
  var year = a.getFullYear();
  var month = monthNames[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = year + ' ' + month + ' ' 
    + (date < 10 ? '0' : '') + date + ',&nbsp;&nbsp;&nbsp;' 
    + (hour < 10 ? '0' : '') + hour + ':' 
    + (min < 10 ? '0' : '') + min + ':' 
    + (sec < 10 ? '0' : '') + sec ;
  return time;
}