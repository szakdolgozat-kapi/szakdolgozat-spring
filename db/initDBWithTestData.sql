-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: szakdolgozat.cnlctydoilf0.us-west-2.rds.amazonaws.com    Database: szakdolgozat
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `candidate_lectures`
--

DROP TABLE IF EXISTS `candidate_lectures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_lectures` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `info` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `place_name` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `start_time_stamp` bigint(20) DEFAULT NULL,
  `end_time_stamp` bigint(20) DEFAULT NULL,
  `image_url` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `event_id` bigint(20) NOT NULL,
  `event_name` varchar(45) COLLATE utf8_hungarian_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `teacher_id` bigint(20) DEFAULT NULL,
  `teacher_name` varchar(255) COLLATE utf8_hungarian_ci DEFAULT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_lectures`
--

LOCK TABLES `candidate_lectures` WRITE;
/*!40000 ALTER TABLE `candidate_lectures` DISABLE KEYS */;
INSERT INTO `candidate_lectures` VALUES (1,'acceptable lecture','info 1','test place 1',1493239315,1493269315,NULL,4,'Test name mod',0,0,NULL,NULL),(2,'delatable lecture','info 2','test place 2',1493239315,1493269315,NULL,2,'Test event 2',0,0,NULL,NULL);
/*!40000 ALTER TABLE `candidate_lectures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_messages`
--

DROP TABLE IF EXISTS `contact_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_messages` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `message` mediumtext COLLATE utf8_hungarian_ci,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_messages`
--

LOCK TABLES `contact_messages` WRITE;
/*!40000 ALTER TABLE `contact_messages` DISABLE KEYS */;
INSERT INTO `contact_messages` VALUES (1,'Kapi Zoltán','kapi.zoli77@gmail.com','','Teszt',0);
/*!40000 ALTER TABLE `contact_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `start_time_stamp` bigint(20) NOT NULL,
  `end_time_stamp` bigint(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Test Event 1','This is a test event info',1484521200,1484780399,0),(2,'Test event 2','This is a test event',1484576801,1484836001,0),(3,'Test event 3','This is a test event',1484922401,1485354401,0),(4,'Test name','Test info',1492336244,1492356304,0),(5,'new event','new event info',1492293600,1492466399,1),(6,'Új esemény','infó',1493654400,1493740800,0);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lectures`
--

DROP TABLE IF EXISTS `lectures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lectures` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `info` text,
  `askaquestion_identifier` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `place_name` varchar(255) DEFAULT NULL,
  `event_id` bigint(20) NOT NULL,
  `start_time_stamp` bigint(20) NOT NULL,
  `end_time_stamp` bigint(20) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `teacher_id` bigint(20) DEFAULT NULL,
  `teacher_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`identifier`),
  KEY `FKeoyxv5pjq4bljo7w5oy79qrkj` (`event_id`),
  CONSTRAINT `FKeoyxv5pjq4bljo7w5oy79qrkj` FOREIGN KEY (`event_id`) REFERENCES `events` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lectures`
--

LOCK TABLES `lectures` WRITE;
/*!40000 ALTER TABLE `lectures` DISABLE KEYS */;
INSERT INTO `lectures` VALUES (1,'Lecture 1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','0006','https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',1,1484576801,1484584001,'',0,NULL,NULL),(2,'Lecture 2','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',1,1484584001,1484590000,'',0,NULL,NULL),(3,'Lecture 3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',1,1484590000,1484596000,'',0,NULL,NULL),(4,'Lecture 4','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','0006','https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484596000,1484602000,'',0,NULL,NULL),(5,'Lecture 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484602000,1484608000,'',0,NULL,NULL),(6,'Lecture 6','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484608000,1484614000,'',0,NULL,NULL),(7,'Lecture 7','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484614000,1484620000,'',0,NULL,NULL),(8,'Lecture 8','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',3,1484922401,1484936801,'',0,NULL,NULL),(9,'Lecture 9','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'','IK-F01',3,1484936741,1484943941,'Test event 3',0,NULL,NULL),(10,'Test name Lecture','Test info',NULL,'127.0.0.1:8080/api/v1/files/1492949573430.png','IK-F02',4,1492336800,1492436740,'Test name mod',0,NULL,NULL),(11,'Teszt előadás','Ez egy teszt előadás',NULL,'127.0.0.1:8080/api/v1/files/1492951604889.png','IK-F01',3,1491638400,1491645600,'Test event 3',0,NULL,NULL),(12,'NEW acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239255,1493269255,'Test name',0,NULL,NULL),(13,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(14,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(15,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(16,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(17,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL);
/*!40000 ALTER TABLE `lectures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) DEFAULT NULL,
  `event_name` varchar(45) DEFAULT NULL,
  `course` varchar(255) DEFAULT NULL,
  `offer` varchar(255) DEFAULT NULL,
  `teacher` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`identifier`),
  KEY `FKmg74xmttuuea0u5ws2rmo2q6u_idx` (`event_id`),
  CONSTRAINT `FKmg74xmttuuea0u5ws2rmo2q6u` FOREIGN KEY (`event_id`) REFERENCES `events` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES (1,1,'Test Event 1','Test course 1','Test offer 1','Test teacher 1',1),(2,1,'Test Event 1','Test course 2','Test offer 2','Test teacher 2',0),(3,1,'Test Event 1','Test course 3','Test offer 3','Test teacher 3',0),(4,2,'Test event 2','Test course 4','Test offer 4','Test teacher 4',0),(5,2,'Test event 2','Test course 5','Test offer 5','Test teacher 5',0),(6,2,'Test event 2','Test course 6','Test offer 6','Test teacher 6',0),(7,2,'Test event 2','Test course 7','Test offer 7','Test teacher 7',0),(8,1,'Test Event 1','','','',1);
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `lecture_code` varchar(45) DEFAULT NULL,
  `question` varchar(45) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  `accepted_date` bigint(20) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,'0001','First question','0001',1492156142,0),(2,'0001','Second question','0002',1492156242,0),(3,'0001','Third question','0003',1492156276,0);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `authority` varchar(45) DEFAULT NULL,
  `company` varchar(45) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Kapi Zoltán','kapi.zoli77@gmail.com','password','SUPERADMIN','',NULL,0,1),(2,'Teszt Elek','b','b','USER','Example Inc.',NULL,0,1),(3,'Admin Antal','a','a','ADMIN','UniDeb',NULL,0,1),(10,'Kapi Zoltán Erik','kapi.zoli77+unislido@gmail.com','jelszo','ADMIN','Debreceni Egyetem',NULL,0,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-03 22:17:57
