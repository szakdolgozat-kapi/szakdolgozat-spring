/**
 * Created by zolikapi on 2017. 04. 14..
 */

$(document).ready(function() {
    loadUserinfo();
});


function loadUserinfo(){
    var userIcon = localStorage.user_icon;
    var userName = localStorage.user_name;
    
    var menu_name = document.getElementById("menu_name");
    var menu_icon = document.getElementById("menu_icon");
    
    var top_name = document.getElementById("top_name");
    var top_icon = document.getElementById("top_icon");
    
    if (localStorage.user_icon) {
        menu_icon.src = localStorage.user_icon;
        top_icon.src = localStorage.user_icon;
    }
    if (localStorage.user_name) {
        menu_name.innerHTML = localStorage.user_name;
        top_name.innerHTML = localStorage.user_name;
    }
}

function logout(){
    if (localStorage.user_icon) {
        localStorage.removeItem('user_icon');
    }
    if (localStorage.user_name) {
        localStorage.removeItem('user_name');
    }
    if (localStorage.login_token) {
        localStorage.removeItem('login_token');
    }
    window.location.href = "login.html";
}

function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var monthNames = ["Január", "Február", "Március", "Április", "Május", "Június",
        "Július", "Augusztus", "Szeptember", "Október", "November", "December"
    ];
    var year = a.getFullYear();
    var month = monthNames[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var time = year + ' ' + month + ' '
        + (date < 10 ? '0' : '') + date + ', '
        + (hour < 10 ? '0' : '') + hour + ':'
        + (min < 10 ? '0' : '') + min;
    return time;
}

function apiErrorHandler(response){
    if (response && response.data && response.data.hashMap && response.data.hashMap.redirectUrl){
        if(typeof response.data.hashMap.redirectUrl !== "undefined"){
            window.location.href = response.data.hashMap.redirectUrl;
        }
    }
}