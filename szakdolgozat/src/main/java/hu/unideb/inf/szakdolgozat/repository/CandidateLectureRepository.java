package hu.unideb.inf.szakdolgozat.repository;

import hu.unideb.inf.szakdolgozat.model.persistence.CandidateLecturePersistence;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zolikapi on 2017. 04. 26..
 */
public interface CandidateLectureRepository extends JpaRepository<CandidateLecturePersistence, Integer> {
    List<CandidateLecturePersistence> findByIdentifier(long identifier);
    List<CandidateLecturePersistence> findByIdentifierIn(List<Long> identifierList);
}
