package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2017. 04. 09..
 */

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Bad user or pass")  // 403
public class BadUserOrPassword extends USException {
    public BadUserOrPassword() {
        status = HttpStatus.FORBIDDEN.value();
        messageEn = R.status.error.message.badUserOrPassword.en;
        messageHu = R.status.error.message.badUserOrPassword.hu;
    }
}
