package hu.unideb.inf.szakdolgozat.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

/**
 * Created by zolikapi on 2017. 04. 26..
 */
@Entity
@Table(name = "candidateLectures")
@Where(clause = "deleted=0")
@JsonIgnoreProperties({"deleted"})
public class CandidateLecturePersistence {
    private long identifier;
    private String name;
    private String info;
    private String placeName;
    private Long startTimeStamp;
    private Long endTimeStamp;
    private String imageUrl;
    private long eventId;
    private String eventName;
    private int status; // 0: waiting, 1: accepted, 2: declined
    private Boolean deleted;
    private Long teacherId;
    private String teacherName;

    public CandidateLecturePersistence() {
    }

    public CandidateLecturePersistence(String name, String info, String placeName, Long startTimeStamp, Long endTimeStamp, String imageUrl, long eventId, String eventName, Boolean deleted) {
        this.name = name;
        this.info = info;
        this.placeName = placeName;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.imageUrl = imageUrl;
        this.eventId = eventId;
        this.eventName = eventName;
        this.deleted = deleted;
    }

    public CandidateLecturePersistence(long identifier, String name, String info, String placeName, Long startTimeStamp, Long endTimeStamp, String imageUrl, long eventId, String eventName, int status, Boolean deleted, Long teacherId, String teacherName) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.placeName = placeName;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.imageUrl = imageUrl;
        this.eventId = eventId;
        this.eventName = eventName;
        this.status = status;
        this.deleted = deleted;
        this.teacherId = teacherId;
        this.teacherName = teacherName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdentifier() {
        return identifier;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public String getPlaceName() {
        return placeName;
    }

    public Long getStartTimeStamp() {
        return startTimeStamp;
    }

    public Long getEndTimeStamp() {
        return endTimeStamp;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public long getEventId() {
        return eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public void setStartTimeStamp(Long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public void setEndTimeStamp(Long endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
}
