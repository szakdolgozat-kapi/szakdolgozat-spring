package hu.unideb.inf.szakdolgozat.repository;

import hu.unideb.inf.szakdolgozat.model.persistence.QuestionPersistence;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zolikapi on 2017. 04. 11..
 */
public interface QuestionRepository extends JpaRepository<QuestionPersistence, Integer> {
    List<QuestionPersistence> findByIdentifier(long identifier);
    List<QuestionPersistence> findByLectureCode(String lectureCode);
}
