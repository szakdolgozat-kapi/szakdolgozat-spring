package hu.unideb.inf.szakdolgozat.repository;

import hu.unideb.inf.szakdolgozat.model.persistence.UserPersistence;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zolikapi on 2017. 04. 09..
 */
public interface UserRepository extends JpaRepository<UserPersistence, Integer> {
    List<UserPersistence> findByIdentifier(long identifier);
    List<UserPersistence> findByEmailAndPassword(String email, String password);
    List<UserPersistence> findByEmail(String email);
    List<UserPersistence> findByAuthority(String authority);
}
