package hu.unideb.inf.szakdolgozat.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by zolikapi on 2017. 03. 28..
 */
@Configuration
public class MyWebMvcConfig {

    @Bean
    public WebMvcConfigurerAdapter forwardToIndex() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName(
                        "redirect:/user/index.html");
                registry.addViewController("/login").setViewName(
                        "redirect:/admin/production/login.html");
                registry.addViewController("/error/403").setViewName(
                        "redirect:/admin/production/page_403.html");
                registry.addViewController("/error/404").setViewName(
                        "redirect:/admin/production/page_404.html");
                registry.addViewController("/error/500").setViewName(
                        "redirect:/admin/production/page_500.html");
                registry.addViewController("/change_password").setViewName(
                        "redirect:/admin/production/change_password.html");

            }
        };
    }

}
