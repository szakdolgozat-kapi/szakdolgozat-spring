package hu.unideb.inf.szakdolgozat.service;

import hu.unideb.inf.szakdolgozat.exception.EventNotFoundException;
import hu.unideb.inf.szakdolgozat.exception.GeneralError;
import hu.unideb.inf.szakdolgozat.model.persistence.EventPersistence;
import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;
import hu.unideb.inf.szakdolgozat.repository.EventRepository;
import hu.unideb.inf.szakdolgozat.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by zolikapi on 2017. 01. 15..
 */
@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private OfferRepository offerRepository;

    public EventPersistence getNextEvent(Boolean withOffers){
        List<EventPersistence> allEvent = getAllEvent(withOffers);
        List<EventPersistence> sortedEvents = allEvent.stream().sorted().collect(Collectors.toList());
        try {
            return sortedEvents.get(sortedEvents.size() - 1);
        }catch (Exception e){
            return null;
        }
    }

    public List<EventPersistence> getAllEvent(Boolean withOffers){
        List<EventPersistence> events = eventRepository.findAll();
        for (EventPersistence e: events) {
            if (withOffers){
                for (LecturePersistence l: e.getLectures()) {
                    l.setOffers(offerRepository.findByEventId(l.getEventId()));
                }
            }
        }
        return events;
    }

    public Boolean createOrUpdateEvent(EventPersistence eventPersistence){
        eventRepository.save(eventPersistence);
        return true;
    }

    public Boolean delete(long identifier){
        List<EventPersistence> list = eventRepository.findByIdentifier(identifier);
        if (list != null && list.size() == 1){
            EventPersistence e = list.get(0);
            e.setDeleted(true);
            eventRepository.save(e);
            return true;
        } else {
            throw new GeneralError();
        }
    }

    public EventPersistence getEvent(Integer id){
        return eventRepository.findOne(id);
    }

}
