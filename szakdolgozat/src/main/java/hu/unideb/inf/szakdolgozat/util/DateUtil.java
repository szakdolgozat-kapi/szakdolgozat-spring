package hu.unideb.inf.szakdolgozat.util;

import java.util.Calendar;

/**
 * Created by zolikapi on 2016. 12. 23..
 */
public class DateUtil {

    public static Calendar getUTCTime(){
        Calendar calendar = Calendar.getInstance();
        return calendar;
    }

    public static long getUTCTimeStamp(){
        return DateUtil.getUTCTime().getTimeInMillis();
    }

    public static long getUTCTimeStampInSecond(){
        return DateUtil.getUTCTime().getTimeInMillis() / 1000;
    }

    public static String getUTCTimeStampAsString(){
        return new Long(DateUtil.getUTCTime().getTimeInMillis()).toString();
    }


}
