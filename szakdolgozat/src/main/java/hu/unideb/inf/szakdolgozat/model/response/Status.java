package hu.unideb.inf.szakdolgozat.model.response;


import hu.unideb.inf.szakdolgozat.constant.R;
import hu.unideb.inf.szakdolgozat.exception.USException;

/**
 * Created by zolikapi on 2016. 12. 10..
 */
public class Status {
    private int status;
    private String messageEn;
    private String messageHu;


    public static Status success(){
        return new Status(200, R.status.succes.en, R.status.succes.hu);
    }

    public static Status generalError(){
        return new Status(500, R.status.error.message.general.en, R.status.error.message.general.hu);
    }

    public Status(int status, String messageEn, String messageHu) {
        this.status = status;
        this.messageEn = messageEn;
        this.messageHu = messageHu;
    }

    public Status(USException exception) {
        this.status = exception.status;
        this.messageEn = exception.messageEn;
        this.messageHu = exception.messageHu;
    }

    public int getStatus() {
        return status;
    }

    public String getMessageEn() {
        return messageEn;
    }

    public String getMessageHu() {
        return messageHu;
    }
}
