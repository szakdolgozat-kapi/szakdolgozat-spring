package hu.unideb.inf.szakdolgozat.service;

import hu.unideb.inf.szakdolgozat.constant.AppConstants;
import hu.unideb.inf.szakdolgozat.exception.BadUserOrPassword;
import hu.unideb.inf.szakdolgozat.exception.ExpiredTokenException;
import hu.unideb.inf.szakdolgozat.exception.GeneralError;
import hu.unideb.inf.szakdolgozat.exception.UserAlreadyExistsException;
import hu.unideb.inf.szakdolgozat.model.persistence.UserPersistence;
import hu.unideb.inf.szakdolgozat.model.response.LoginResponse;
import hu.unideb.inf.szakdolgozat.repository.UserRepository;
import hu.unideb.inf.szakdolgozat.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import java.util.*;
import java.util.stream.Collectors;

import static hu.unideb.inf.szakdolgozat.model.response.LoginResponse.Authority.ADMIN;
import static hu.unideb.inf.szakdolgozat.model.response.LoginResponse.Authority.SUPERADMIN;
import static hu.unideb.inf.szakdolgozat.model.response.LoginResponse.Authority.USER;

/**
 * Created by zolikapi on 2017. 04. 09..
 */

@Service
public class LoginService {

    private Map<String, String> loginTokens = new HashMap<>();

    @Autowired
    private UserRepository userRepository;

    public LoginResponse login(String email, String pass){
        List<UserPersistence> list = userRepository.findByEmailAndPassword(email,pass);
        if (list == null || list.size() != 1){
            throw new BadUserOrPassword();
        }else{
            UserPersistence user = list.get(0);
            String iconURL;
            String redirectUrl;
            if (user.getAuthority().equalsIgnoreCase(LoginResponse.Authority.ADMIN.toString()) ||
                    user.getAuthority().equalsIgnoreCase(LoginResponse.Authority.SUPERADMIN.toString())){
                redirectUrl = "/admin/production/admin_index.html";
            }else {
                redirectUrl = "/admin/production/teacher_index.html";
            }
            if (user.getIcon() != null && !user.getIcon().isEmpty()){
                iconURL = user.getIcon();
            }else {
                //default Image
                iconURL = "/admin/production/images/user.png";
            }

            LoginResponse loginResponse = new LoginResponse(redirectUrl,
                    LoginResponse.Authority.USER,
                    iconURL,
                    user.getUserName());
            return loginResponse;
        }
    }

    public LoginResponse register(String name, String email, String phone, String pass, String company, String imageUrl){
        List<UserPersistence> list = userRepository.findByEmail(email);
        if (list != null && list.size() == 1){
            //User already exists
            throw new UserAlreadyExistsException();
        }else{
            //Save user
            UserPersistence u = new UserPersistence(name, email, phone, pass, LoginResponse.Authority.USER.toString(), company, imageUrl);
            userRepository.save(u);

            //default Image
            String iconURL;
            if (imageUrl == null || imageUrl.isEmpty()){
                iconURL = "/admin/production/images/user.png";
            } else{
                iconURL = imageUrl;
            }

            LoginResponse loginResponse = new LoginResponse("/admin/production/teacher_index.html",
                    LoginResponse.Authority.USER,
                    iconURL,
                    name);
            return loginResponse;
        }
    }

    public Boolean deleteUser(long identifier){
        List<UserPersistence> list = userRepository.findByIdentifier(identifier);
        if (list != null && list.size() == 1){
            UserPersistence u = list.get(0);
            u.setDeleted(true);
            userRepository.save(u);
            return true;
        } else {
            throw new GeneralError();
        }
    }

    public Boolean deleteUser(String email){
        List<UserPersistence> list = userRepository.findByEmail(email);
        if (list != null && list.size() == 1){
            UserPersistence u = list.get(0);
            u.setDeleted(true);
            userRepository.save(u);
            return true;
        } else {
            throw new GeneralError();
        }
    }

    public Boolean createOrModifyUser(UserPersistence userPersistence){
        userRepository.save(userPersistence);
        return true;
    }

    public Map<String, Long> getUserMap(){
        Map<String, Long> userMap = new HashMap<>();

        List<UserPersistence> allUser = userRepository.findAll();
        long adminCount = allUser.stream().filter(u -> (u.getAuthority().equals(ADMIN.toString()) || u.getAuthority().equals(SUPERADMIN.toString())) && u.getActive()).count();
        long teacherCount = allUser.stream().filter(u -> u.getAuthority().equals(USER.toString()) && u.getActive()).count();

        userMap.put("admin", adminCount);
        userMap.put("teacher", teacherCount);

        return userMap;
    }

    public List<UserPersistence> getAllAdmin(){
        List<UserPersistence> list = new ArrayList<>();
        list.addAll(userRepository.findByAuthority(ADMIN.toString()));
        list.addAll(userRepository.findByAuthority(SUPERADMIN.toString()));
        return list;
    }

    public List<UserPersistence> getAllTeacher(){
        return userRepository.findByAuthority(USER.toString());
    }

    public String createLoginToken(String email){
        String token = this.newLoginCookieValue(email);
        this.loginTokens.put(email, token);
        return token;
    }

    public Boolean deleteLoginToken(String token){
        for(Map.Entry<String, String> entry : loginTokens.entrySet()){
            if (entry.getValue().equals(token)){
                this.loginTokens.remove(entry.getKey());
                break;
            }
        }
        return true;
    }

    public Boolean isTokenValid(String token){
        Optional<String > t = this.loginTokens.values().stream().filter(c -> c.equals(token)).findFirst();
        if (t.isPresent()){
            return true;
        }else{
            throw new BadUserOrPassword();
        }
    }

    public UserPersistence getUserByToken(String token){
        List<String> emailList = this.loginTokens.entrySet()
                .stream()
                .filter(entry -> Objects.equals(entry.getValue(), token))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        if (emailList != null && emailList.size() == 1){
            List<UserPersistence> userPersistenceList = userRepository.findByEmail(emailList.get(0));
            if (userPersistenceList != null && userPersistenceList.size() == 1){
                return userPersistenceList.get(0);
            }
            throw new BadUserOrPassword();
        }else{
            throw new BadUserOrPassword();
        }
    }

    public UserPersistence getUserByEmail(String email){
        List<UserPersistence> userPersistenceList = userRepository.findByEmail(email);
        if (userPersistenceList != null && userPersistenceList.size() == 1){
            return userPersistenceList.get(0);
        }
        throw new BadUserOrPassword();
    }

    public String newLoginCookieValue(String email){
        return new Integer ((email + DateUtil.getUTCTimeStampAsString()).hashCode()).toString();
    }
}
