// Contact Form Scripts

$(function() {

    $("#contactForm input,#contactForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var name = $("input#user-name").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var message = $("textarea#message").val();

            params = {name: name, email: email, phone : phone, message : message};

            console.log(params);

            $.post('/api/v1/sendMessage', params,
                function(response){
                    console.log(response);
                    if (response.status.status === 200){

                        //clear all fields
                        $('#contactForm').trigger("reset");

                    }else{
                        apiErrorHandler(response);
                    }
                }, 'json');
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
