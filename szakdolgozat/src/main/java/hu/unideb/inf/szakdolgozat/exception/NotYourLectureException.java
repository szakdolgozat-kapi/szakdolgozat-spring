package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2016. 12. 23..
 */

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Not your Lecture")  // 403
public class NotYourLectureException extends USException {
    public NotYourLectureException(){
        status = HttpStatus.FORBIDDEN.value();
        messageEn = R.status.error.message.notYourLecture.en;
        messageHu = R.status.error.message.notYourLecture.hu;
    }
}
