/**
 * Created by zolikapi on 2017. 04. 09..
 */


function login(){
    var email = document.getElementById("loginEmail").value;
    var pass = document.getElementById("loginPassword").value;
    
    $.post('/api/v1/login', { email: email, pass : pass},
     function(response){
        console.log(response);
        if (response.status.status === 200){
            localStorage.setItem("user_icon", response.data.loginResponse.iconUrl);
            localStorage.setItem("user_name", response.data.loginResponse.userName);
            localStorage.setItem("login_token", response.data.loginResponse.loginToken);
            
            window.location.href = response.data.loginResponse.redirectUrl;
        }else{
            alert(response.status.messageHu);
        }
    }, 'json');
}

function sendRegister(imageUrl) {
    var name = document.getElementById("registerName").value;
    var email = document.getElementById("registerEmail").value;
    var phone = document.getElementById("registerPhone").value;
    var pass = document.getElementById("registerPassword").value;
    var company = document.getElementById("registerCompany").value;

    $.post('/api/v1/register', { name: name, email: email, phone: phone, pass : pass, company: company, imageUrl: imageUrl},
        function(response){
            console.log(response);
            if (response.status.status === 200){
                localStorage.setItem("user_icon", response.data.loginResponse.iconUrl);
                localStorage.setItem("user_name", response.data.loginResponse.userName);
                localStorage.setItem("login_token", response.data.loginResponse.loginToken);

                window.location.href = response.data.loginResponse.redirectUrl;
            }else{
                alert(response.status.messageHu);
            }
        }, 'json');
}

function register(){
    var image = document.getElementById("input-image");
    if (image.files.length == 0){
        sendRegister(null);
    }else{
        //TODO: validation, resize
        uploadImage();
    }
}

function uploadImage(){

    var image = document.getElementById("input-image");
    var file = image.files[0];
    var formData = new FormData();
    formData.append("file", file);

    $.ajax({
        url: '/api/v1/uploadFile',
        type: 'POST',

        data: formData,

        cache: false,
        contentType: false,
        processData: false,

        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            myXhr.onreadystatechange = function() {
                if (myXhr.readyState == 4 && myXhr.status == 200) {
                    var jsonResponse = JSON.parse(myXhr.responseText);
                    if (jsonResponse.status.status === 200){
                        console.log(myXhr.responseText); // handle response.
                        var imageUrl = jsonResponse.data.string;
                        console.log(imageUrl); // handle response.
                        sendRegister(imageUrl);
                    }
                }
            };

            return myXhr;
        },
    });
}