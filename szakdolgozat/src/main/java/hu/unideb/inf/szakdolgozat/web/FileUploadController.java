package hu.unideb.inf.szakdolgozat.web;

import hu.unideb.inf.szakdolgozat.model.response.ResponseDataWrapper;
import hu.unideb.inf.szakdolgozat.model.response.USResponse;
import hu.unideb.inf.szakdolgozat.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static hu.unideb.inf.szakdolgozat.constant.AppConstants.apiv1Prefix;

/**
 * Created by zolikapi on 2017. 04. 22..
 */
@RestController
public class FileUploadController {

    private final StorageService storageService;

    @Autowired
    public FileUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @CrossOrigin
    @RequestMapping(value = apiv1Prefix + "/uploadFile", method= RequestMethod.POST)
    public USResponse handleFileUpload(@RequestParam("file") MultipartFile file,
                                       HttpServletRequest request) throws IOException {

        //TODO: create full url with domain
        StringBuilder sb = new StringBuilder();
        sb.append("http://")
        .append(request.getServerName())
        .append(":")
        .append(request.getServerPort())
        .append("/")
        .append(apiv1Prefix)
        .append("/files/")
        .append(storageService.store(file));
        String url = sb.toString();

        ResponseDataWrapper data = new ResponseDataWrapper(url);

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value = apiv1Prefix + "/files/{filename:.+}", method= RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
                .ok()
                .body(file);
    }
}
