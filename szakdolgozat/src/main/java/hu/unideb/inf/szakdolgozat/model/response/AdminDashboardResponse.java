package hu.unideb.inf.szakdolgozat.model.response;

import hu.unideb.inf.szakdolgozat.model.persistence.CandidateLecturePersistence;
import hu.unideb.inf.szakdolgozat.model.persistence.ContactMessagePersistence;
import hu.unideb.inf.szakdolgozat.model.persistence.EventPersistence;
import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;

import java.util.List;
import java.util.Map;

/**
 * Created by zolikapi on 2017. 05. 01..
 */
public class AdminDashboardResponse {
    EventPersistence nextEvent;
    LecturePersistence nextLecture;
    List<CandidateLecturePersistence> candidateLectures;
    Map<String, Long> userMap;
    List<ContactMessagePersistence> contactMessageList;

    public AdminDashboardResponse(EventPersistence nextEvent, LecturePersistence nextLecture, List<CandidateLecturePersistence> candidateLectures, Map<String, Long> userMap, List<ContactMessagePersistence> contactMessageList) {
        this.nextEvent = nextEvent;
        this.nextLecture = nextLecture;
        this.candidateLectures = candidateLectures;
        this.userMap = userMap;
        this.contactMessageList = contactMessageList;
    }

    public EventPersistence getNextEvent() {
        return nextEvent;
    }

    public void setNextEvent(EventPersistence nextEvent) {
        this.nextEvent = nextEvent;
    }

    public LecturePersistence getNextLecture() {
        return nextLecture;
    }

    public void setNextLecture(LecturePersistence nextLecture) {
        this.nextLecture = nextLecture;
    }

    public List<CandidateLecturePersistence> getCandidateLectures() {
        return candidateLectures;
    }

    public void setCandidateLectures(List<CandidateLecturePersistence> candidateLectures) {
        this.candidateLectures = candidateLectures;
    }

    public Map<String, Long> getUserMap() {
        return userMap;
    }

    public void setUserMap(Map<String, Long> userMap) {
        this.userMap = userMap;
    }

    public List<ContactMessagePersistence> getContactMessageList() {
        return contactMessageList;
    }

    public void setContactMessageList(List<ContactMessagePersistence> contactMessageList) {
        this.contactMessageList = contactMessageList;
    }
}
