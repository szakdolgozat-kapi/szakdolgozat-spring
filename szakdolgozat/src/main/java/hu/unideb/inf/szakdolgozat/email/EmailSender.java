package hu.unideb.inf.szakdolgozat.email;

import com.google.common.collect.Lists;
import it.ozimov.springboot.mail.model.Email;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import it.ozimov.springboot.mail.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.SendFailedException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import static hu.unideb.inf.szakdolgozat.constant.AppConstants.noreplayEmail;

/**
 * Created by zolikapi on 2017. 05. 01..
 */

@Service
public class EmailSender {

    @Autowired
    public EmailService emailService;

    public void sendEmail(String to, String subject, String body) throws SendFailedException {
        try {
            Email email = DefaultEmail.builder()
                    .from(new InternetAddress(noreplayEmail))
                    .to(Lists.newArrayList(new InternetAddress(to)))
                    .subject(subject)
                    .body(body)
                    .encoding("UTF-8").build();

            emailService.send(email);
        } catch (AddressException e) {
            e.printStackTrace();
        }
    }
}
