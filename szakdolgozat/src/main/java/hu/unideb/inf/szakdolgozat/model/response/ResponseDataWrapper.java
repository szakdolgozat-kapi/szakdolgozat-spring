package hu.unideb.inf.szakdolgozat.model.response;

import java.util.HashMap;
import java.util.List;

/**
 * Created by zolikapi on 2016. 12. 13..
 */
public class ResponseDataWrapper extends HashMap<String, Object>{

    public ResponseDataWrapper() {
        super();
    }

    public ResponseDataWrapper(Object object) {
        super();
        String className = object.getClass().getSimpleName();
        String key = Character.toLowerCase(className.charAt(0)) + className.substring(1);
        put(key, object);
    }

    public void addObject(Object object){
        String className = object.getClass().getSimpleName();
        String key = Character.toLowerCase(className.charAt(0)) + className.substring(1);
        put(key, object);
    }

    public ResponseDataWrapper(List<Object> objects) {
        super();
        objects.stream().forEach(o -> {
            addObject(o);
//            String className = o.getClass().getSimpleName();
//            String key = Character.toLowerCase(className.charAt(0)) + className.substring(1);
//            put(key, o);
        });
    }
}
