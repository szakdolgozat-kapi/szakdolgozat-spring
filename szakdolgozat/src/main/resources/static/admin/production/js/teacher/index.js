/**
 * Created by zolikapi on 2017. 04. 30..
 */

$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/teacherDashboard?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var lectures = new Array;

                response.data.teacherDashboardResponse.lectures.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });

                reloadLectures(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});


function reloadLectures(lectures) {
    //empty list
    $('#lectures-table tbody').empty();
    var tableBody = document.getElementById("candidate-lectures-tBody");

    for (i  in lectures) {
        var lecture = lectures[i];

        var cellCount = tableBody.rows.length;
        var row = tableBody.insertRow(cellCount);
        row.id = event.identifier;
        if (i % 2 === 0){
            row.classList.toggle('even');
        }else{
            row.classList.toggle('odd');
        }

        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);

        cell0.innerHTML = lecture.name;
        cell1.innerHTML = lecture.info;
        cell2.innerHTML = "US-"  + lecture.identifier;

        /*row.addEventListener('click', function (event) {
            //TODO: show modal with details like on calendar page

        });*/
    }
}
