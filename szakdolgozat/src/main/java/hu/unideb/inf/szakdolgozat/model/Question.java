package hu.unideb.inf.szakdolgozat.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hu.unideb.inf.szakdolgozat.util.DateUtil;


/**
 * Created by zolikapi on 2016. 12. 13..
 */

@JsonIgnoreProperties({"questionerUUID"})
public class Question implements Comparable<Question> {
    public String id;
    public String lectureId;
    public String questionString;
    public String questionerUUID;

    public Question(String lectureId, String questionString, String questionerUUID) {
        this.id = DateUtil.getUTCTimeStampAsString();
        this.lectureId = lectureId;
        this.questionString = questionString;
        this.questionerUUID = questionerUUID;
    }

    @Override
    public int compareTo(Question o) {
        return this.id.compareTo(o.id);
    }
}
