/**
 * Created by zolikapi on 2017. 04. 14..
 */

var selectedEvent = null;
var allEvent = null;
var deletableEventId = null;

$(document).ready(function() {
    moment.locale('hu', {
         week: { dow: 1 }, // Monday is the first day of the week
        format: 'YYYY.MM.DD HH:mm'
    });

    $('input[name="reservation-time"]').daterangepicker(
        {
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 5,
            locale: {
                format: 'YYYY.MM.DD HH:mm'
            },
            startDate: '2019.04.01 00:00',
            endDate: '2019.04.01 00:00',
            weekStart: 6
        },
        function(start, end, label) {
            console.log("A new date range was chosen: " + start.format('YYYY.MM.DD HH:mm') + ' to ' + end.format('YYYY.MM.DD HH:mm'));
        });

    NProgress.start();
    $.get('/api/v1/allEvent?loginToken=' + localStorage.login_token,
         function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);
                
                var events = new Array();
                response.data.arrayList.forEach(function(event) {
                    events[event.identifier] = event;
                });
                reloadData(events);
                
            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});

function reloadData(events) {
    allEvent = events;

    //delete all rows from table
    var table = $('#datatable').DataTable();
    table
        .clear()
        .draw();

    var date = $("#reservation-time").data('daterangepicker');
    date.setStartDate(new Date());
    date.setEndDate(new Date());

    for (id in events) {
        var event = events[id];
        addRow(event);
    }
}

function submitNewEvent() {
    var name = document.getElementById("name-input");
    var info = document.getElementById("info-input");
    var date = $("#reservation-time").data('daterangepicker');
    
    var params;
    if (selectedEvent != null){
        params = {loginToken: localStorage.login_token, identifier : selectedEvent.identifier, name : name.value, info : info.value, startDate : date.startDate.format('X'), endDate : date.endDate.format('X') };
    }else{
        params = {loginToken: localStorage.login_token, name : name.value, info : info.value, startDate : date.startDate.format('X'), endDate : date.endDate.format('X') };
    }
    
    NProgress.start();
    $.post('/api/v1/createOrUpdateEvent', params,
     function(response){
        NProgress.done();
        console.log(response);
        if (response.status.status === 200){

            var events = new Array;
            response.data.arrayList.forEach(function(event) {
                events[event.identifier] = event;
            });

            var name = document.getElementById("name-input");
            var info = document.getElementById("info-input");
            name.value = "";
            info.value = "";

            reloadData(events);
                
        }else{
            apiErrorHandler(response);
        }
    }, 'json');
}

function editEvent(id){
    var event = allEvent[id];

    if (event){
        selectedEvent = event;
        //fill the form
        var name = document.getElementById("name-input");
        var info = document.getElementById("info-input");

        name.value = event.name;
        info.value = event.info;

        var date = $("#reservation-time").data('daterangepicker');
        date.setStartDate(new Date(event.startTimeStamp * 1000));
        date.setEndDate(new Date(event.endTimeStamp * 1000));


        var divPosition = $('#event-form').offset();
        $('html, body').animate({scrollTop: divPosition.top}, "slow");
    }else{
        alert('Hiba történt!');
    }

}

function sendDeletableEvent(){
    var params = {loginToken: localStorage.login_token, identifier : deletableEventId};
    
    NProgress.start();
    $.post('/api/v1/deleteEvent', params,
     function(response){
        NProgress.done();
        console.log(response);
        if (response.status.status === 200){

            var events = new Array;
            response.data.arrayList.forEach(function(event) {
                events[event.identifier] = event;
            });

            var name = document.getElementById("name-input");
            var info = document.getElementById("info-input");
            name.value = "";
            info.value = "";

            reloadData(events);
                
        }else{
            apiErrorHandler(response);
        }
    }, 'json');
}

function deleteEvent(id){
    deletableEventId = id;
}

function addRow(event){
    var t = $('#datatable').DataTable();
    var edit = "<button type=\"button\" class=\"btn btn-info btn-xs\" href=\"#\" onclick=\"editEvent('" +
        event.identifier +
        "');return false;\"><i class=\"fa fa-edit m-right-xs\"></i> Szerkeszt</button>" +
        "<button type=\"button\" class=\"btn btn-danger btn-xs\" data-toggle=\"modal\" data-target=\".bs-delete-modal-lg\" href=\"#\" onclick=\"deleteEvent('" +
        event.identifier +
        "');return false;\">Törlés</button>";

    t.row.add( [
        event.name,
        event.info,
        timeConverter(event.startTimeStamp),
        timeConverter(event.endTimeStamp),
        edit
    ] ).draw( false );

}

