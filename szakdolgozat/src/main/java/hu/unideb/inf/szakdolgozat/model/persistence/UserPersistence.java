package hu.unideb.inf.szakdolgozat.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Created by zolikapi on 2017. 04. 09..
 */

@Entity
@Table(name = "users")
@Where(clause = "deleted=0")
@JsonIgnoreProperties({"deleted"})
public class UserPersistence {
    private long identifier;
    private String userName;
    private String email;
    private String phone;
    private String password;
    private String authority;
    private String company;
    private String icon;
    private Boolean deleted;
    private Boolean active;

    public UserPersistence() {
        this.deleted = deleted;
        this.active = true;
    }

    public UserPersistence(long identifier, String userName, String email, String password, String authority, String company, String icon, Boolean deleted) {
        this.identifier = identifier;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.authority = authority;
        this.company = company;
        this.icon = icon;
        this.deleted = deleted;
        this.active = true;
    }

    public UserPersistence(String name, String email, String password, String authority, String company) {
        this.userName = name;
        this.email = email;
        this.password = password;
        this.authority = authority;
        this.company = company;
        this.deleted = false;
        this.active = true;
    }

    public UserPersistence(String name, String email, String phone, String password, String authority, String company, String icon) {
        this.userName = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.authority = authority;
        this.company = company;
        this.icon = icon;
        this.deleted = false;
        this.active = true;
    }

    public UserPersistence(String name, String email, String authority, String company, Boolean active) {
        this.userName = name;
        this.email = email;
        this.authority = authority;
        this.company = company;
        this.deleted = false;
        this.active = active;
    }

    public UserPersistence(String name, String email, String imageUrl) {
        this.userName = name;
        this.email = email;
        this.icon = imageUrl;
        this.deleted = false;
        this.active = true;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdentifier() {
        return identifier;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAuthority() {
        return authority;
    }

    public String getCompany() {
        return company;
    }

    public String getIcon() {
        return icon;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
