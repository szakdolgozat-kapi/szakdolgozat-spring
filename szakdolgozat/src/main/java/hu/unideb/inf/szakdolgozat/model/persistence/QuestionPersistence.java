package hu.unideb.inf.szakdolgozat.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hu.unideb.inf.szakdolgozat.model.Question;
import hu.unideb.inf.szakdolgozat.util.DateUtil;
import javafx.beans.DefaultProperty;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Created by zolikapi on 2017. 04. 11..
 */
@Entity
@Table(name = "questions")
@Where(clause = "deleted=0")
@JsonIgnoreProperties({"deleted"})
public class QuestionPersistence {

    private long identifier;
    private String lectureCode;
    private String question;
    private String uuid;
    private long acceptedDate;
    private Boolean deleted;

    public QuestionPersistence() {
    }

    public QuestionPersistence(Question q) {
        this.lectureCode = q.lectureId;
        this.question = q.questionString;
        this.uuid = q.questionerUUID;
        this.acceptedDate = DateUtil.getUTCTimeStamp();
        this.deleted = false;
    }

    public QuestionPersistence(long identifier, String lectureCode, String question, String uuid, long acceptedDate, Boolean deleted) {
        this.identifier = identifier;
        this.lectureCode = lectureCode;
        this.question = question;
        this.uuid = uuid;
        this.acceptedDate = acceptedDate;
        this.deleted = deleted;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdentifier() {
        return identifier;
    }

    public String getLectureCode() {
        return lectureCode;
    }

    public String getQuestion() {
        return question;
    }

    public String getUuid() {
        return uuid;
    }

    public long getAcceptedDate() {
        return acceptedDate;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public void setLectureCode(String lectureCode) {
        this.lectureCode = lectureCode;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setAcceptedDate(long acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
