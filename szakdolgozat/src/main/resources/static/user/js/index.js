/**
 * Created by zolikapi on 2017. 04. 12..
 */

var allEvent = null;

$(document).ready(function() {
    $.get('/api/v1/events',
        function(response){
            if (response.status.status === 200){
                console.log(response);

                var events = new Array();
                response.data.arrayList.forEach(function(event) {
                    events[event.identifier] = event;
                });
                setupSelect(events);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});

function trackLecture() {
    var lectureId = document.getElementById("lectureId").value;
    document.cookie = "trackingLectureId="+lectureId;
    window.location.href = "./tracking.html";
}

function setupSelect(events) {
    allEvent = events;
    $("#event-selector").empty();

    var select = document.getElementById('event-selector');

    for (id in events) {
        var event = events[id];

        var opt = document.createElement('option');
        opt.value = id;
        opt.innerHTML = event.name;
        select.appendChild(opt);
    }
    selectOnChange();
}

function selectOnChange() {
    var e = document.getElementById('event-selector');
    var selectedEventId = e.options[e.selectedIndex].value;
    reloadTable(selectedEventId);

}

function reloadTable(eventId) {
    var event = allEvent[eventId];
    if (typeof event === 'undefined'){
        alert('Hiba történt!');
        return;
    }

    $('#lecture-table tbody').empty();
    var tableBody = document.getElementById("lecture-list-body");

    for (i  in event.lectures) {
        var lecture = event.lectures[i];

        var cellCount = tableBody.rows.length;
        var row = tableBody.insertRow(cellCount);
        row.id = event.identifier;
        if (i % 2 === 0){
            row.classList.toggle('even');
        }else{
            row.classList.toggle('odd');
        }

        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);

        cell0.innerHTML = lecture.name;
        cell1.innerHTML = lecture.info;
        cell2.innerHTML = lecture.placeName;
        cell3.innerHTML = timeConverter(lecture.startTimeStamp);
        cell4.innerHTML = timeConverter(lecture.endTimeStamp);

    }
}

function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var monthNames = ["Január", "Február", "Március", "Április", "Május", "Június",
        "Július", "Augusztus", "Szeptember", "Október", "November", "December"
    ];
    var year = a.getFullYear();
    var month = monthNames[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var time = year + ' ' + month + ' '
        + (date < 10 ? '0' : '') + date + ',&nbsp;&nbsp;&nbsp;'
        + (hour < 10 ? '0' : '') + hour + ':'
        + (min < 10 ? '0' : '') + min;
    return time;
}
