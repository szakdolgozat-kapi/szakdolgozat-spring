package hu.unideb.inf.szakdolgozat.service;

import hu.unideb.inf.szakdolgozat.constant.AppConstants;
import hu.unideb.inf.szakdolgozat.exception.GeneralError;
import hu.unideb.inf.szakdolgozat.exception.LectureNotFoundException;
import hu.unideb.inf.szakdolgozat.exception.NotYourLectureException;
import hu.unideb.inf.szakdolgozat.exception.QuestionNotFoundException;
import hu.unideb.inf.szakdolgozat.model.Lecture;
import hu.unideb.inf.szakdolgozat.model.Question;
import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;
import hu.unideb.inf.szakdolgozat.model.persistence.QuestionPersistence;
import hu.unideb.inf.szakdolgozat.repository.LectureRepository;
import hu.unideb.inf.szakdolgozat.repository.OfferRepository;
import hu.unideb.inf.szakdolgozat.repository.QuestionRepository;
import hu.unideb.inf.szakdolgozat.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static hu.unideb.inf.szakdolgozat.util.DateUtil.getUTCTimeStamp;

/**
 * Created by zolikapi on 2017. 01. 15..
 */

@Service
public class LectureService {

    @Autowired
    private LectureRepository lectureRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private OfferRepository offerRepository;

    private TreeMap<String, Lecture> lectureMap = new TreeMap<String, Lecture>(); // id - lecture
    private Map<String, TreeSet<Question>> questionMap = new LinkedHashMap<String, TreeSet<Question>>(); // lectureID - question array
    private Map<String, Question> acceptedQuestions = new LinkedHashMap<String, Question>(); // lectureID - accepted question
    private TreeSet<String> reusableIds = new TreeSet<>();
    private Timer timer;
    private static long schedulerDelay = 3600000; //1 hour

    public LectureService() {
        this.startScheduler();


        //TODO: delete this
        lectureMap.put("0001", new Lecture("0001", "1st Lecture", "info for 1st Lecture", "Test Teacher", ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond(), ZonedDateTime.now(ZoneOffset.UTC).plus(100, ChronoUnit.MINUTES).toEpochSecond()));
        lectureMap.put("0002", new Lecture("0002", "2nd Lecture", "info for 2nd Lecture", "Test Teacher", ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond(), ZonedDateTime.now(ZoneOffset.UTC).plus(100, ChronoUnit.MINUTES).toEpochSecond()));
        lectureMap.put("0003", new Lecture("0003", "3rd Lecture", "info for 3rd Lecture", "Test Teacher", ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond(), ZonedDateTime.now(ZoneOffset.UTC).plus(100, ChronoUnit.MINUTES).toEpochSecond()));
        lectureMap.put("0004", new Lecture("0004", "4th Lecture", "info for 4th Lecture", "Test Teacher", ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond(), ZonedDateTime.now(ZoneOffset.UTC).plus(100, ChronoUnit.MINUTES).toEpochSecond()));
        lectureMap.put("0005", new Lecture("0005", "5th Lecture", "info for 5th Lecture", "Test Teacher", ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond(), ZonedDateTime.now(ZoneOffset.UTC).plus(100, ChronoUnit.MINUTES).toEpochSecond()));

        acceptedQuestions.put("0001", new Question("0001", "Hello! Ez egy teszt kérdés.", "testUUID"));
    }

    public void addLecture(String key, Lecture lecture){
        lectureMap.put(key, lecture);
    }

    public Map<String, Lecture> getLectureMap() {
        return lectureMap;
    }

    public Lecture getLecture(String id){
        if (lectureMap.containsKey(id)){
            return lectureMap.get(id);
        }else{
            throw new LectureNotFoundException();
        }
    }

    public LecturePersistence getNextLecture(){
        List<LecturePersistence> allLecture = getAllLecture();
        List<LecturePersistence> sortedLectures = allLecture.stream().sorted().collect(Collectors.toList());
        try {
            return sortedLectures.get(sortedLectures.size() - 1);
        }catch (Exception e){
            return null;
        }
    }

    public void bookNextLectureId(String uuid, LecturePersistence lecturePersistence){
        String nextLectureId = "0000";

        //check if is there a reusable id
        if (this.reusableIds.size() > 0){
            String reusableId = this.reusableIds.first();
            this.reusableIds.remove(reusableId);
            nextLectureId = StringUtil.addZerosToLectureID(reusableId);
            //else get the next id
        } else if (this.lectureMap.size() > 0){
            String lastID = this.lectureMap.lastKey();
            nextLectureId = StringUtil.getNextLectureID(lastID);
        }

        lecturePersistence.setAskAQuestionIdentifier(nextLectureId);
        lectureRepository.save(lecturePersistence);

        Lecture newLecture = new Lecture(nextLectureId, uuid);
        newLecture.setInfo(lecturePersistence);
        this.lectureMap.put(nextLectureId, newLecture);
        this.questionMap.put(nextLectureId, new TreeSet<>());
    }

    public Boolean canIAsk(String uuid, String lectureId){
        long now = getUTCTimeStamp();
        TreeSet<Question> questions = this.questionMap.get(lectureId);
        List<Question> filteredList = questions.stream()
                .filter(question -> question.questionerUUID.equals(uuid) &&
                        now - Long.parseLong(question.id) < AppConstants.questionLimitTime)
                .collect(Collectors.toList());

        if (filteredList != null && filteredList.size() > 0){
            return false;
        }else{
            return true;
        }
    }


    public Boolean askAQuestion(String uuid, String lectureId, String question){
        TreeSet<Question> questions = new TreeSet<Question>();
        if (this.questionMap.containsKey(lectureId)){
            questions = this.questionMap.get(lectureId);
        }else{
            this.questionMap.put(lectureId, questions);
        }

        Question newQuestion = new Question(lectureId, question, uuid);
        questions.add(newQuestion);

        return true;
    }

    public TreeSet<Question> getQuestions(String uuid, String lectureId){
        checkLectureOwner(uuid, lectureId); // if it's false, throw an exception

        TreeSet<Question> questions = new TreeSet<>();
        if (this.questionMap.containsKey(lectureId)){
            questions = this.questionMap.get(lectureId);
        }

        return questions;
    }

    public List<QuestionPersistence> getAllAcceptedQuestion(String lectureCode){

        List<QuestionPersistence> list = questionRepository.findByLectureCode(lectureCode);
        if (list != null && list.size() > 0){
            return list;
        }else{
            throw new QuestionNotFoundException();
        }
    }

    public Boolean delete(long identifier){
        List<QuestionPersistence> list = questionRepository.findByIdentifier(identifier);
        if (list != null && list.size() == 1){
            QuestionPersistence q = list.get(0);
            q.setDeleted(true);
            questionRepository.save(q);
            return true;
        } else {
            throw new GeneralError();
        }
    }

    public ArrayList<Question> getNewQuestions(String uuid, String lectureId, String lastQuestionId){
        checkLectureOwner(uuid, lectureId); // if it's false, throw an exception

        TreeSet<Question> questions = getQuestions(lectureId); // if it's not exsists, throw an exception

        if (lastQuestionId.isEmpty()){
            return new ArrayList<Question>(questions);
        }

        try {
            Iterator<Question> iterator = questions.iterator();
            ArrayList<Question> newQuestions = new ArrayList<>();
            while (iterator.hasNext()){
                Question q = iterator.next();

                if (q.id.compareTo(lastQuestionId) > 0){
                    newQuestions.add(iterator.next());
                    break;
                }

            }

            return newQuestions;

        }catch (NoSuchElementException | NullPointerException e){
            throw new QuestionNotFoundException();
        }
    }

    public Boolean acceptAQuestion(String uuid, String lectureId, String questionId){
        checkLectureOwner(uuid, lectureId); // if it's false, throw an exception

        TreeSet<Question> questions = getQuestions(lectureId); // if it's not exsists, throw an exception

        Question acceptedQuestion = null;

        Iterator<Question> iterator = questions.iterator();
        while (iterator.hasNext()){
            Question q = iterator.next();

            if (q.id.equals(questionId)){
                QuestionPersistence questionPersistence = new QuestionPersistence(q);
                questionRepository.save(questionPersistence);

                acceptedQuestion = q;
                break;
            }
        }

        if (acceptedQuestion == null){
            throw new QuestionNotFoundException();
        }

        acceptedQuestions.put(lectureId, acceptedQuestion);
        return true;

    }

    public Boolean declineAQuestion(String uuid, String lectureId, String questionId){
        checkLectureOwner(uuid, lectureId); // if it's false, throw an exception

        TreeSet<Question> questions = getQuestions(lectureId); // if it's not exsists, throw an exception

        Question declinedQuestion = null;

        Iterator<Question> iterator = questions.iterator();
        while (iterator.hasNext()){
            Question q = iterator.next();

            if (q.id.equals(questionId)){
                declinedQuestion = q;
                break;
            }
        }

        if (declinedQuestion == null){
            throw new QuestionNotFoundException();
        }

        questions.remove(declinedQuestion);
        this.acceptedQuestions.remove(lectureId);
        return true;

    }

    public Boolean finishLecture(String uuid, String lectureId){
        checkLectureOwner(uuid, lectureId); // if it's false, throw an exception

        List<LecturePersistence> list = lectureRepository.findByAskAQuestionIdentifier(lectureId);
        if (list != null && list.size() > 0){
            Iterator<LecturePersistence> iterator = list.iterator();
            while (iterator.hasNext()){
                LecturePersistence lecturePersistence = iterator.next();
                lecturePersistence.setAskAQuestionIdentifier(null);
                lectureRepository.save(lecturePersistence);
            }
        }

        this.lectureMap.remove(lectureId);
        this.questionMap.remove(lectureId);
        this.acceptedQuestions.remove(lectureId);

        // reuse the ID
        reusableIds.add(lectureId);

        return true;
    }

    public Question getAcceptedQuestion(String lectureId){
        Question acceptedQuestion = acceptedQuestions.get(lectureId);
        if (acceptedQuestion != null){
            return acceptedQuestion;
        }else{
            return null;
        }
    }

    /*
    * Private methods
    * */

    private Boolean checkLectureOwner(String uuid, String lectureID){
        Lecture lecture = getLecture(lectureID);  // if it's not exsists, throw an exception
        return  checkLectureOwner(uuid, lecture);
    }

    private Boolean checkLectureOwner(String uuid, Lecture lecture){
        if (lecture.getTeacherUUID().equals(uuid)){
            return true;
        }else{
            throw new NotYourLectureException();
        }
    }

    private TreeSet<Question>getQuestions(String lectureId){
        if (this.questionMap.containsKey(lectureId)){
            return this.questionMap.get(lectureId);
        }else{
            throw new LectureNotFoundException();
        }
    }

    /*
    * Method with persisted models
    * */

    public List<LecturePersistence> getAllLecture(){
        List<LecturePersistence> lectures = lectureRepository.findAll();
        for (LecturePersistence l: lectures) {
            l.setOffers(offerRepository.findByEventId(l.getEventId()));
        }
        return lectures;
    }

    public Boolean createOrUpdateLecture(LecturePersistence lecturePersistence){
        lectureRepository.save(lecturePersistence);
        return true;
    }

    public LecturePersistence getPersistedLectureByIdentifier(long identifier){
        List<LecturePersistence> list = lectureRepository.findByIdentifier(identifier);
        if (list == null || list.size() != 1){
            throw new LectureNotFoundException();
        }
        return list.get(0);
    }

    public LecturePersistence getPersistedLectureByAskAQuestionIdentifier(String askAQuestionIdentifier){
        List<LecturePersistence> list = lectureRepository.findByAskAQuestionIdentifier(askAQuestionIdentifier);
        if (list == null || list.size() != 1){
            throw new LectureNotFoundException();
        }
        return list.get(0);
    }

    public List<LecturePersistence> getPersistedLectureByTeacherIdentifier(long teacherIdentifier){
        return lectureRepository.findByTeacherId(teacherIdentifier);
    }

    /*
    * Private methods for scheduler
    */
    private synchronized void startScheduler() {
        if (this.timer != null){
            this.timer.cancel(); //this will cancel the current task.
        }

        this.timer = new Timer();

        TimerTask action = new TimerTask() {
            public void run() {
                LectureService.this.clearMaps();
            }

        };

        this.timer.schedule(action, schedulerDelay); //this starts the task
    }

    private void clearMaps(){
        this.startScheduler(); //restart the scheduler

        // clear the Lecture and the Question map

        //remove outdated lectures and questions
        long now = getUTCTimeStamp();
        Iterator<Lecture> iterator = this.lectureMap.values().iterator();
        while (iterator.hasNext()){
            Lecture lecture = iterator.next();

            if (lecture.getEndDate() < now){
                iterator.remove();
                this.questionMap.remove(lecture.getIdentifier());
                this.acceptedQuestions.remove(lecture.getIdentifier());

                //set null at database
                List<LecturePersistence> list = lectureRepository.findByAskAQuestionIdentifier(lecture.getIdentifier());
                if (list != null && list.size() > 0){
                    Iterator<LecturePersistence> iterator1 = list.iterator();
                    while (iterator1.hasNext()){
                        LecturePersistence lecturePersistence = iterator1.next();
                        lecturePersistence.setAskAQuestionIdentifier(null);
                        lectureRepository.save(lecturePersistence);
                    }
                }

                // reuse the ID
                reusableIds.add(lecture.getIdentifier());
            }
        }

    }
}