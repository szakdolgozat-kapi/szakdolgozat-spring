package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2017. 04. 19..
 */
@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Internal Server Error")  // 500
public class GeneralError extends USException {
    public GeneralError() {
        status = HttpStatus.INTERNAL_SERVER_ERROR.value();
        messageEn = R.status.error.message.general.en;
        messageHu = R.status.error.message.general.hu;
    }
}
