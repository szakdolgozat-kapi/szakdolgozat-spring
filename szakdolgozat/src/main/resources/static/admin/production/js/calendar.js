/**
 * Created by zolikapi on 2017. 04. 14..
 */

var allLecture = null;

$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/allLecture?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var lectures = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });
                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});

function reloadData(lectures) {
    allLecture = lectures;

    //delete all event from calendar
    var calendar = $('#calendar');
    calendar.fullCalendar( 'removeEvents');

    for (id in lectures) {
        var lecture = lectures[id];

        calendar.fullCalendar('renderEvent', {
                id: lecture.identifier,
                title: lecture.name,
                start: new Date(lecture.startTimeStamp * 1000),
                end: new Date(lecture.endTimeStamp * 1000),
                allDay: false
            },
            true // make the event "stick"
        );
    }
}

function setupEventModal(calEvent) {
    var lecture = allLecture[calEvent.id];

    if (lecture){
        var lectureName = document.getElementById("lecture-name");
        var info = document.getElementById("lecture-info");
        var eventName = document.getElementById("event-name");
        var placeName = document.getElementById("place-name");
        var time = document.getElementById("lecure-time");

        lectureName.value = lecture.name;
        info.value = lecture.info;
        eventName.value = lecture.eventName;
        placeName.value = lecture.placeName;
        time.value = timeConverter(lecture.startTimeStamp) + " - " + timeConverter(lecture.endTimeStamp);
    }
}