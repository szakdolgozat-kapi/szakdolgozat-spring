/**
 * Created by zolikapi on 2017. 04. 30..
 */


$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/adminDashboard?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var nextEvent = response.data.adminDashboardResponse.nextEvent;
                var nextLecture = response.data.adminDashboardResponse.nextLecture;
                var userMap = response.data.adminDashboardResponse.userMap;
                var candidateLectures = new Array();
                var contactMessages = new Array;

                response.data.adminDashboardResponse.candidateLectures.forEach(function(lecture) {
                    candidateLectures[lecture.identifier] = lecture;
                });
                response.data.adminDashboardResponse.contactMessageList.forEach(function(message) {
                    contactMessages[message.identifier] = message;
                });

                reloadCandidateLectures(candidateLectures);
                reloadUsers(userMap);
                reloadNextEvent(nextEvent);
                reloadNextLecture(nextLecture);
                reloadContactMessages(contactMessages);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});


function reloadCandidateLectures(candidateLectures) {
    //empty list
    $('#candidata-lectures-table tbody').empty();
    var tableBody = document.getElementById("candidata-lectures-tBody");

    for (i  in candidateLectures) {
        var lecture = candidateLectures[i];

        var cellCount = tableBody.rows.length;
        var row = tableBody.insertRow(cellCount);
        row.id = event.identifier;
        if (i % 2 === 0){
            row.classList.toggle('even');
        }else{
            row.classList.toggle('odd');
        }

        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);

        cell0.innerHTML = lecture.name;
        cell1.innerHTML = lecture.info;

        row.addEventListener('click', function (event) {
            window.location.href = "/admin_candidate_lectures.html";
        });
    }
}

function reloadUsers(users) {
    var userDiv = document.getElementById("user-list");

    var adminNode = document.createElement('h4');
    adminNode.innerHTML = "admin: " + users['admin'];
    userDiv.appendChild(adminNode);

    var teacherNode = document.createElement('h4');
    teacherNode.innerHTML = "tanár: " + users['teacher'];
    userDiv.appendChild(teacherNode);
}

function reloadNextEvent(nextEvent) {
    //TODO: null event
    var nextEventDiv = document.getElementById("next-event");

    var nameTitleNode = document.createElement('h4');
    nameTitleNode.innerHTML = "Előadás:"
    nextEventDiv.appendChild(nameTitleNode);
    var nameNode = document.createElement('h7');
    nameNode.innerHTML = nextEvent.name;
    nextEventDiv.appendChild(nameNode);

    var infoTitleNode = document.createElement('h4');
    infoTitleNode.innerHTML = "Infó:"
    nextEventDiv.appendChild(infoTitleNode);
    var infoNode = document.createElement('h7');
    infoNode.innerHTML = nextEvent.info;
    nextEventDiv.appendChild(infoNode);

    var dateTitleNode = document.createElement('h4');
    dateTitleNode.innerHTML = "Időpont:"
    nextEventDiv.appendChild(dateTitleNode);
    var dateNode = document.createElement('h7');
    dateNode.innerHTML = timeConverter(nextEvent.startTimeStamp) + ' - ' + timeConverter(nextEvent.endTimeStamp);
    nextEventDiv.appendChild(dateNode);
}

function reloadNextLecture(nextLecture) {
    //TODO: null lecture
    var nextLectureDiv = document.getElementById("next-lecture");

    var nameTitleNode = document.createElement('h4');
    nameTitleNode.innerHTML = "Cím:"
    nextLectureDiv.appendChild(nameTitleNode);
    var nameNode = document.createElement('h7');
    nameNode.innerHTML = nextLecture.name;
    nextLectureDiv.appendChild(nameNode);

    var infoTitleNode = document.createElement('h4');
    infoTitleNode.innerHTML = "Infó:"
    nextLectureDiv.appendChild(infoTitleNode);
    var infoNode = document.createElement('h7');
    infoNode.innerHTML = nextLecture.info;
    nextLectureDiv.appendChild(infoNode);

    var eventTitleNode = document.createElement('h4');
    eventTitleNode.innerHTML = "Esemény:"
    nextLectureDiv.appendChild(eventTitleNode);
    var eventNode = document.createElement('h7');
    eventNode.innerHTML = nextLecture.eventName;
    nextLectureDiv.appendChild(eventNode);

    var placeTitleNode = document.createElement('h4');
    placeTitleNode.innerHTML = "Helye:"
    nextLectureDiv.appendChild(placeTitleNode);
    var placeNode = document.createElement('h7');
    placeNode.innerHTML = nextLecture.placeName;
    nextLectureDiv.appendChild(placeNode);

    var dateTitleNode = document.createElement('h4');
    dateTitleNode.innerHTML = "Időpont:"
    nextLectureDiv.appendChild(dateTitleNode);
    var dateNode = document.createElement('h7');
    dateNode.innerHTML = timeConverter(nextLecture.startTimeStamp) + ' - ' + timeConverter(nextLecture.endTimeStamp);
    nextLectureDiv.appendChild(dateNode);
}

function reloadContactMessages(contactMessages) {
    //empty list
    $('#message-table tbody').empty();
    var tableBody = document.getElementById("message-tBody");

    for (i  in contactMessages) {
        var message = contactMessages[i];

        var cellCount = tableBody.rows.length;
        var row = tableBody.insertRow(cellCount);
        row.id = event.identifier;
        if (i % 2 === 0){
            row.classList.toggle('even');
        }else{
            row.classList.toggle('odd');
        }

        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);

        cell0.innerHTML = message.name;
        cell1.innerHTML = message.email
        cell2.innerHTML = message.phone;
        cell3.innerHTML = message.message;

    }
}


function sendNewAdmin() {
    console.log('sendNewAdmin');

    var name = document.getElementById("name-input");
    var email = document.getElementById("email-input");

    params = {loginToken: localStorage.login_token, name: name.value, email: email.value};
    NProgress.start();
    $.post('/api/v1/addNewAdmin', params,
        function(response){
            NProgress.done();
            console.log(response);
            if (response.status.status === 200){

                name.value = "";
                email.value = "";

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
}