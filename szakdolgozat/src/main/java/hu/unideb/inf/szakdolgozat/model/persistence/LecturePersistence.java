package hu.unideb.inf.szakdolgozat.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by zolikapi on 2017. 01. 15..
 */

@Entity
@Table(name = "lectures")
@Where(clause = "deleted=0")
@JsonIgnoreProperties({"deleted"})
public class LecturePersistence implements Comparable<LecturePersistence> {
    private long identifier;
    private String name;
    private String info;
    private String placeName;
    private Long startTimeStamp;
    private Long endTimeStamp;
    private String imageUrl;
    private String askAQuestionIdentifier;
    @Transient
    private List<OfferPersistence> offers;
    private long eventId;
    private String eventName;
    private Boolean deleted;
    private Long teacherId;
    private String teacherName;

    public LecturePersistence() {
        this.deleted = false;
    }

    public LecturePersistence(String name, String info, String placeName, Long startTimeStamp, Long endTimeStamp, String imageUrl, long eventId, String eventName, Long teacherId, String teacherName) {
        this.name = name;
        this.info = info;
        this.placeName = placeName;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.imageUrl = imageUrl;
        this.eventId = eventId;
        this.eventName = eventName;
        this.deleted = false;
        this.teacherId = teacherId;
        this.teacherName = teacherName;
    }

    public LecturePersistence(long identifier, String name, String info, String placeName, Long startTimeStamp, Long endTimeStamp, String imageUrl, long eventId, String eventName) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.placeName = placeName;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.imageUrl = imageUrl;
        this.eventId = eventId;
        this.eventName = eventName;
        this.deleted = false;
    }

    public LecturePersistence(String name, String info, String placeName, Long startTimeStamp, Long endTimeStamp, String imageUrl, String askAQuestionIdentifier) {
        this.name = name;
        this.info = info;
        this.placeName = placeName;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.imageUrl = imageUrl;
        this.askAQuestionIdentifier = askAQuestionIdentifier;
        this.deleted = false;
    }

    public LecturePersistence(long identifier, String name, String info, String placeName, Long startTimeStamp, Long endTimeStamp, String imageUrl, String askAQuestionIdentifier, List<OfferPersistence> offers, long eventId, String eventName, Boolean deleted) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.placeName = placeName;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.imageUrl = imageUrl;
        this.askAQuestionIdentifier = askAQuestionIdentifier;
        this.offers = offers;
        this.eventId = eventId;
        this.eventName = eventName;
        this.deleted = deleted;
    }

    public LecturePersistence(CandidateLecturePersistence candidateLecture) {
        this.name = candidateLecture.getName();
        this.info = candidateLecture.getInfo();
        this.placeName = candidateLecture.getPlaceName();
        this.startTimeStamp = candidateLecture.getStartTimeStamp();
        this.endTimeStamp = candidateLecture.getEndTimeStamp();
        this.imageUrl = candidateLecture.getImageUrl();
        this.eventId = candidateLecture.getEventId();
        this.eventName = candidateLecture.getEventName();
        this.deleted = false;
        this.teacherId = candidateLecture.getTeacherId();
        this.teacherName = candidateLecture.getTeacherName();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdentifier() {
        return identifier;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public String getPlaceName() {
        return placeName;
    }

    public Long getStartTimeStamp() {
        return startTimeStamp;
    }

    public Long getEndTimeStamp() {
        return endTimeStamp;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getAskAQuestionIdentifier() {
        return askAQuestionIdentifier;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public void setStartTimeStamp(Long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public void setEndTimeStamp(Long endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setAskAQuestionIdentifier(String askAQuestionIdentifier) {
        this.askAQuestionIdentifier = askAQuestionIdentifier;
    }

    @Transient
    public List<OfferPersistence> getOffers() {
        return offers;
    }

    @Transient
    public void setOffers(List<OfferPersistence> offers) {
        this.offers = offers;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    @Override
    public String toString() {
        return "LecturePersistence{" +
                "identifier=" + identifier +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", placeName='" + placeName + '\'' +
                ", startTimeStamp=" + startTimeStamp +
                ", endTimeStamp=" + endTimeStamp +
                ", imageUrl='" + imageUrl + '\'' +
                ", askAQuestionIdentifier='" + askAQuestionIdentifier + '\'' +
                ", offers=" + offers +
                ", eventId=" + eventId +
                ", eventName='" + eventName + '\'' +
                '}';
    }

    @Override
    public int compareTo(LecturePersistence o) {
        return this.startTimeStamp.compareTo(o.getStartTimeStamp());
    }
}
