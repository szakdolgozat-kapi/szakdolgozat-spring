package hu.unideb.inf.szakdolgozat.util;

import hu.unideb.inf.szakdolgozat.constant.AppConstants;
import hu.unideb.inf.szakdolgozat.exception.TooManyLectureException;

/**
 * Created by zolikapi on 2016. 12. 23..
 */
public class StringUtil {

    public static String removeZeros(String string){
        return string.replaceFirst("^0+(?!$)", "");
    }

    public static Integer removeZerosAsInt(String string){
        String s = StringUtil.removeZeros(string);
        return new Integer(s);
    }

    public static String addZeros(Integer number, int length){
        String s = number.toString();
        StringBuilder sb = new StringBuilder();
        for (int i = length - s.length(); i > 0; i-- ){
            sb.append("0");
        }
        sb.append(s);
        return sb.toString();
    }

    public static String addZerosToLectureID(Integer number){
        if (number.intValue() <= AppConstants.maximumNOLecture){
            int length = (new Integer(AppConstants.maximumNOLecture)).toString().length();
            return StringUtil.addZeros(number, length);
        }else {
            throw new TooManyLectureException();
        }
    }

    public static String addZerosToLectureID(String numberAsString){

        try {
            int number = new Integer(numberAsString).intValue();

            if (number <= AppConstants.maximumNOLecture){
                int length = (new Integer(AppConstants.maximumNOLecture)).toString().length();
                return StringUtil.addZeros(number, length);
            }else {
                throw new TooManyLectureException();
            }
        }catch (NumberFormatException e){
            throw new TooManyLectureException();
        }
    }

    public static String getNextLectureID(String id){
        Integer actualID = StringUtil.removeZerosAsInt(id);
        actualID++;
        return StringUtil.addZerosToLectureID(actualID);
    }
}
