package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2016. 12. 23..
 */

@ResponseStatus(value= HttpStatus.UNPROCESSABLE_ENTITY, reason="Too many Lecture")  // 422
public class TooManyLectureException extends USException{
    public TooManyLectureException() {
        status = HttpStatus.UNPROCESSABLE_ENTITY.value();
        messageEn = R.status.error.message.tooManyLecture.en;
        messageHu = R.status.error.message.tooManyLecture.hu;
    }
}

