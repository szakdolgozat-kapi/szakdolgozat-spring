package hu.unideb.inf.szakdolgozat.model.response;


import javax.servlet.http.Cookie;
import java.util.ArrayList;

/**
 * Created by zolikapi on 2016. 12. 10..
 */
public class USResponse {
    private Status status;
    private Object data;

    public static USResponse successResponse(Object o){
        return new USResponse(Status.success(), o);
    }

    public USResponse(Status status, Object data) {
        this.status = status;
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }
}
