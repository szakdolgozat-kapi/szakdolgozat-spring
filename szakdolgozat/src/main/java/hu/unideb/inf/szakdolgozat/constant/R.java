package hu.unideb.inf.szakdolgozat.constant;

/**
 * Created by zolikapi on 2017. 01. 15..
 */
public final class R {
    public static final class status {

        public static final class error {

            public static final class message {

                public static final class general {

                    public static final String en = "An error occurred.";
                    public static final String hu = "Hiba történt.";

                }

                public static final class invalidEmail {

                    public static final String en = "Please check the email address!";
                    public static final String hu = "Ellenőrizze az email címet!";

                }

                public static final class lectureNotFound {

                    public static final String en = "No such Lecture!";
                    public static final String hu = "Nincs ilyen kurzus!";

                }

                public static final class eventNotFound {

                    public static final String en = "No such Event!";
                    public static final String hu = "Nincs ilyen esemény!";

                }

                public static final class tooManyLecture {

                    public static final String en = "There are no free capacity on our server!";
                    public static final String hu = "Nincs szabad kapacitás a szerverünkön!";

                }

                public static final class notYourLecture {

                    public static final String en = "This is not your Lecture, you cannot modify!";
                    public static final String hu = "Ez nem a Te kurzusod, nem módosíthatod!";

                }

                public static final class questionNotFound {

                    public static final String en = "No such Question!";
                    public static final String hu = "Nincs kérdés!";

                }

                public static final class badUserOrPassword {

                    public static final String en = "Bad user or password!";
                    public static final String hu = "Helytelen felhasználónév vagy jelszó!";

                }

                public static final class expiredToken {

                    public static final String en = "Expired login token!";
                    public static final String hu = "Lejárt bejelentkezési token!";

                }

                public static final class userAlreadyExists {

                    public static final String en = "Email already registered!";
                    public static final String hu = "Ezzel az email címmel már regisztráltak a rendszerben!";

                }
            }
        }

        public static final class succes {

            public static final String en = "Success";
            public static final String hu = "Sikeres";

        }
    }
}
