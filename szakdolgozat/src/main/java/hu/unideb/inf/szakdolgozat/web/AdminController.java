package hu.unideb.inf.szakdolgozat.web;

import hu.unideb.inf.szakdolgozat.constant.R;
import hu.unideb.inf.szakdolgozat.email.EmailSender;
import hu.unideb.inf.szakdolgozat.exception.*;
import hu.unideb.inf.szakdolgozat.model.persistence.*;
import hu.unideb.inf.szakdolgozat.model.response.*;
import hu.unideb.inf.szakdolgozat.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.SendFailedException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hu.unideb.inf.szakdolgozat.constant.AppConstants.*;
import static hu.unideb.inf.szakdolgozat.model.response.LoginResponse.Authority.ADMIN;
import static hu.unideb.inf.szakdolgozat.model.response.LoginResponse.Authority.SUPERADMIN;
import static hu.unideb.inf.szakdolgozat.model.response.LoginResponse.Authority.USER;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by zolikapi on 2017. 04. 14..
 */

@RestController
public class AdminController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private LectureService lectureService;
    @Autowired
    private EventService eventService;
    @Autowired
    private OfferService offerService;
    @Autowired
    private CandidateLectureService candidateLectureService;
    @Autowired
    private ContactMessageService contactMessageService;

    @Autowired
    private EmailSender emailSender;

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/allEvent", method=GET)
    public USResponse getAllEvent(@RequestParam(value="loginToken") String token) {

        validateToken(token);

        ResponseDataWrapper data = new ResponseDataWrapper(eventService.getAllEvent(false));

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/createOrUpdateEvent", method=POST)
    public USResponse createOrUpdateEvent(@RequestParam(value="loginToken") String token,
                                          @RequestParam(value="identifier", required = false, defaultValue = "-1")long identifier,
                                          @RequestParam(value="name")String name,
                                          @RequestParam(value="info")String info,
                                          @RequestParam(value="startDate")long startDate,
                                          @RequestParam(value="endDate")long endDate) {

        validateToken(token);
        EventPersistence eventPersistence;
        if (identifier == -1){
            eventPersistence = new EventPersistence(name, info,startDate, endDate);
        }else{
            eventPersistence = new EventPersistence(identifier, name, info,startDate, endDate);
        }
        eventService.createOrUpdateEvent(eventPersistence);

        ResponseDataWrapper data = new ResponseDataWrapper(eventService.getAllEvent(false));

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/createOrUpdateLecture", method=POST)
    public USResponse createOrUpdateLecture(@RequestParam(value="loginToken") String token,
                                            @RequestParam(value="identifier", required = false, defaultValue = "-1")long identifier,
                                            @RequestParam(value="name")String name,
                                            @RequestParam(value="info")String info,
                                            @RequestParam(value="imageUrl", required = false)String imageUrl ,
                                            @RequestParam(value="placeName")String placeName,
                                            @RequestParam(value="eventId")long eventId,
                                            @RequestParam(value="startDate")long startTimeStamp,
                                            @RequestParam(value="endDate")long endTimeStamp,
                                            @RequestParam(value="eventName")String eventName,
                                            @RequestParam(value = "teacherId", required = false)long teacherId,
                                            @RequestParam(value="teacherName")String teacherName) {

        validateToken(token);

        LecturePersistence lecturePersistence;
        if (identifier == -1){
            lecturePersistence = new LecturePersistence(name, info, placeName, startTimeStamp, endTimeStamp, imageUrl, eventId, eventName, teacherId, teacherName);
        }else{
            lecturePersistence = lectureService.getPersistedLectureByIdentifier(identifier);

            lecturePersistence.setName(name);
            lecturePersistence.setInfo(info);
            lecturePersistence.setPlaceName(placeName);
            lecturePersistence.setStartTimeStamp(startTimeStamp);
            lecturePersistence.setEndTimeStamp(endTimeStamp);
            lecturePersistence.setEventId(eventId);
            lecturePersistence.setEventName(eventName);
            lecturePersistence.setTeacherId(teacherId);
            lecturePersistence.setTeacherName(teacherName);

            if (imageUrl != null && !imageUrl.isEmpty()){
                lecturePersistence.setImageUrl(imageUrl);
            }
        }
        lectureService.createOrUpdateLecture(lecturePersistence);


        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getAllLecture());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/registerOwnLecture", method=POST)
    public USResponse registerOwnLecture(@RequestParam(value="loginToken") String token,
                                            @RequestParam(value="name")String name,
                                            @RequestParam(value="info")String info,
                                            @RequestParam(value="imageUrl")String imageUrl ,
                                            @RequestParam(value="placeName")String placeName,
                                            @RequestParam(value="eventId")long eventId,
                                            @RequestParam(value="startDate")long startTimeStamp,
                                            @RequestParam(value="endDate")long endTimeStamp,
                                            @RequestParam(value="eventName")String eventName) {

        validateToken(token);
        UserPersistence user = loginService.getUserByToken(token);

        LecturePersistence lecturePersistence = new LecturePersistence(name, info, placeName, startTimeStamp, endTimeStamp, imageUrl, eventId, eventName, user.getIdentifier(), user.getUserName());
        lectureService.createOrUpdateLecture(lecturePersistence);


        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getAllLecture());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/candidataLectures", method=GET)
    public USResponse acceptCandidataLectures(@RequestParam(value="loginToken") String token) {

        validateToken(token);
        checkAuthority(token, ADMIN);

        ResponseDataWrapper data = new ResponseDataWrapper(candidateLectureService.getAllWaitingCandidateLecture());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/acceptCandidataLectures", method=POST)
    public USResponse acceptCandidataLectures(@RequestParam(value="loginToken") String token,
                                              @RequestParam(value="identifierList[]")Long[] identifierList) {

        validateToken(token);
        checkAuthority(token, ADMIN);

        candidateLectureService.acceptCandidateLectures(identifierList, token);
        ResponseDataWrapper data = new ResponseDataWrapper(candidateLectureService.getAllWaitingCandidateLecture());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/declineCandidataLectures", method=POST)
    public USResponse declineCandidataLectures(@RequestParam(value="loginToken") String token,
                                               @RequestParam(value="identifierList[]")Long[] identifierList) {

        validateToken(token);
        checkAuthority(token, ADMIN);

        candidateLectureService.declineCandidateLectures(identifierList);
        ResponseDataWrapper data = new ResponseDataWrapper(candidateLectureService.getAllWaitingCandidateLecture());

        return USResponse.successResponse(data);
    }


    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/allLecture", method=GET)
    public USResponse getAllLecture(@RequestParam(value="loginToken") String token) {

        validateToken(token);

        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getAllLecture());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/allOffer", method=GET)
    public USResponse getAllOffer(@RequestParam(value="loginToken") String token) {

        validateToken(token);

        ResponseDataWrapper data = new ResponseDataWrapper(offerService.getAllOffer());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/createOrUpdateOffer", method=POST)
    public USResponse createOrUpdateOffer(@RequestParam(value="loginToken") String token,
                                          @RequestParam(value="identifier", required = false, defaultValue = "-1")long identifier,
                                          @RequestParam(value="teacher")String teacher,
                                          @RequestParam(value="offer")String offer,
                                          @RequestParam(value="course")String course,
                                          @RequestParam(value="eventId")long eventId,
                                          @RequestParam(value="eventName")String eventName) {

        System.out.print("[createOrUpdateOffer] loginToken: " + token + " eventId: " + eventId);

        validateToken(token);
        OfferPersistence offerPersistence;
        if (identifier == -1){
            offerPersistence = new OfferPersistence(teacher, course, offer, eventId, eventName);
        }else{
            offerPersistence = new OfferPersistence(identifier, teacher, course, offer, eventId, eventName);
        }
        System.out.print(offerPersistence.toString());
        offerService.createOrUpdateOffer(offerPersistence);

        ResponseDataWrapper data = new ResponseDataWrapper(offerService.getAllOffer());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/allAdmin", method=GET)
    public USResponse getAllAdmin(@RequestParam(value="loginToken") String token) {

        validateToken(token);

        ResponseDataWrapper data = new ResponseDataWrapper(loginService.getAllAdmin());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/allTeacher", method=GET)
    public USResponse getAllTeacher(@RequestParam(value="loginToken") String token) {

        validateToken(token);
        ResponseDataWrapper data = new ResponseDataWrapper(loginService.getAllTeacher());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/deleteEvent", method=POST)
    public USResponse deleteEvent(@RequestParam(value="loginToken") String token,
                                          @RequestParam(value="identifier")long identifier) {

        validateToken(token);
        checkAuthority(token, ADMIN);
        eventService.delete(identifier);

        ResponseDataWrapper data = new ResponseDataWrapper(eventService.getAllEvent(false));

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/deleteLecture", method=POST)
    public USResponse deleteLecture(@RequestParam(value="loginToken") String token,
                                          @RequestParam(value="identifier")long identifier) {

        validateToken(token);
        checkAuthority(token, ADMIN);
        lectureService.delete(identifier);

        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getAllLecture());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/deleteAdmin", method=POST)
    public USResponse deleteAdmin(@RequestParam(value="loginToken") String token,
                                        @RequestParam(value="identifier")long identifier) {

        validateToken(token);
        checkAuthority(token, SUPERADMIN);
        loginService.deleteUser(identifier);

        ResponseDataWrapper data = new ResponseDataWrapper(loginService.getAllAdmin());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/deleteUser", method=POST)
    public USResponse deleteUser(@RequestParam(value="loginToken") String token,
                                  @RequestParam(value="identifier")long identifier) {

        validateToken(token);
        checkAuthority(token, ADMIN);
        loginService.deleteUser(identifier);

        ResponseDataWrapper data = new ResponseDataWrapper(loginService.getAllAdmin());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/deleteOffer", method=POST)
    public USResponse deleteOffer(@RequestParam(value="loginToken") String token,
                                 @RequestParam(value="identifier")long identifier) {

        validateToken(token);
        checkAuthority(token, ADMIN);

        offerService.delete(identifier);

        ResponseDataWrapper data = new ResponseDataWrapper(offerService.getAllOffer());

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/sendMessage", method=POST)
    public USResponse sendMessage(@RequestParam(value="name")String name,
                                  @RequestParam(value="email")String email,
                                  @RequestParam(value="phone")String phone,
                                  @RequestParam(value="message")String message) {

        ContactMessagePersistence contactMessagePersistence = new ContactMessagePersistence(name, email, phone, message);

        ResponseDataWrapper data = new ResponseDataWrapper(contactMessageService.saveMessage(contactMessagePersistence));

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/addNewAdmin", method=POST)
    public USResponse addNewAdmin(@RequestParam(value="loginToken") String token,
                                  @RequestParam(value="name")String name,
                                  @RequestParam(value="email")String email) {

        validateToken(token);
        checkAuthority(token, ADMIN);

        UserPersistence userPersistence = new UserPersistence(name, email, LoginResponse.Authority.ADMIN.toString(), defaultAdminCompany, false);

        ResponseDataWrapper data = new ResponseDataWrapper(loginService.createOrModifyUser(userPersistence));

        try {
            emailSender.sendEmail(email, registerEmailSubject, registerAdminBody);
        }catch (SendFailedException exception) {
            //delete the user and token is already created
            try {
                loginService.deleteUser(email);
            } catch (GeneralError error) {
                //there is no saved user yet
            }

            throw new InvalidEmailException();
        }

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/modifyUser", method=POST)
    public USResponse modifyUser(@RequestParam(value="loginToken") String token,
                                 @RequestParam(value="name")String name,
                                 @RequestParam(value="email")String email,
                                 @RequestParam(value="imageUrl")String imageUrl) {

        validateToken(token);
        checkAuthority(token, USER);

        UserPersistence u = loginService.getUserByToken(token);
        u.setUserName(name);
        u.setEmail(email);
        u.setIcon(imageUrl);

        ResponseDataWrapper data = new ResponseDataWrapper(loginService.createOrModifyUser(u));

        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/changePassword", method=POST)
    public USResponse modifyUser(@RequestParam(value="email")String email,
                                 @RequestParam(value="password")String password) {

        UserPersistence u = loginService.getUserByEmail(email);
        u.setPassword(password);
        u.setActive(true);
        loginService.createOrModifyUser(u);

        LoginResponse loginResponse = loginService.login(email, password);

        loginResponse.loginToken = loginService.createLoginToken(email);

        ResponseDataWrapper data = new ResponseDataWrapper(loginResponse);
        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/adminDashboard", method=GET)
    public USResponse adminDashboard(@RequestParam(value="loginToken") String token) {

        validateToken(token);
        checkAuthority(token, ADMIN);

        EventPersistence nextEvent = eventService.getNextEvent(false);
        LecturePersistence nextLecture = lectureService.getNextLecture();
        List<CandidateLecturePersistence> candidateLectures = candidateLectureService.getAllWaitingCandidateLecture();
        Map<String, Long> userMap = loginService.getUserMap();
        List<ContactMessagePersistence> contactMessageList = contactMessageService.getAllContactMessage();

        AdminDashboardResponse adminDashboardResponse = new AdminDashboardResponse(nextEvent, nextLecture, candidateLectures, userMap, contactMessageList);
        ResponseDataWrapper data = new ResponseDataWrapper(adminDashboardResponse);
        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/teacherDashboard", method=GET)
    public USResponse teacherDashboard(@RequestParam(value="loginToken") String token) {

        validateToken(token);
        UserPersistence user = checkAuthority(token, USER);

        List<LecturePersistence> lectures = lectureService.getPersistedLectureByTeacherIdentifier(user.getIdentifier());
        System.out.println(lectures.toString());

        TeacherDashboardResponse teacherDashboardResponse = new TeacherDashboardResponse(lectures);

        ResponseDataWrapper data = new ResponseDataWrapper(teacherDashboardResponse);

        return USResponse.successResponse(data);
    }

    private void validateToken(String token){
        loginService.isTokenValid(token);
    }

    private UserPersistence checkAuthority(String token, LoginResponse.Authority minAuthority){
        UserPersistence user = loginService.getUserByToken(token);

        switch (minAuthority){
            case USER:
                //access granted
                break;
            case ADMIN:
                if (user.getAuthority().equalsIgnoreCase(USER.toString())){
                    throw new PermissionDenied();
                }
                break;
            case SUPERADMIN:
                if (!user.getAuthority().equalsIgnoreCase(SUPERADMIN.toString())){
                    throw new PermissionDenied();
                }
                break;
        }
        return user;
    }

    @ExceptionHandler(Exception.class)
    public USResponse handleError(Exception ex) {
        ex.printStackTrace();

        Status status = null;
        if (USException.class.isAssignableFrom(ex.getClass())){
            status = new Status((USException)ex);
        }else{
            status = Status.generalError();
        }

        ResponseDataWrapper data = null;

        //TODO: handle 404, 500
        if (PermissionDenied.class.isAssignableFrom(ex.getClass())){
            Map<String, String> map = new HashMap<>();
            map.put("redirectUrl", "/error/403");

            data = new ResponseDataWrapper(map);
        } else if (BadUserOrPassword.class.isAssignableFrom(ex.getClass())){
            Map<String, String> map = new HashMap<>();
            map.put("redirectUrl", "/login");

            data = new ResponseDataWrapper(map);
        }else if (ex.getMessage() != null){
            data = new ResponseDataWrapper(ex.getMessage());
        } else {
            data = new ResponseDataWrapper(ex.toString());
        }

        USResponse response = new USResponse(status, data);
        return response;
    }
}
