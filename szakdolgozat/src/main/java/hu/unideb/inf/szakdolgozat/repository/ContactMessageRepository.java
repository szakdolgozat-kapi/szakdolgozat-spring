package hu.unideb.inf.szakdolgozat.repository;

import hu.unideb.inf.szakdolgozat.model.persistence.ContactMessagePersistence;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by zolikapi on 2017. 04. 30..
 */
public interface ContactMessageRepository extends JpaRepository<ContactMessagePersistence, Integer> {
}
