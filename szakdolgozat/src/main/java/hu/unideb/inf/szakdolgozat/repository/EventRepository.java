package hu.unideb.inf.szakdolgozat.repository;

import hu.unideb.inf.szakdolgozat.model.persistence.EventPersistence;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zolikapi on 2017. 01. 15..
 */
public interface EventRepository extends JpaRepository<EventPersistence, Integer> {
    List<EventPersistence> findByIdentifier(long identifier);
}
