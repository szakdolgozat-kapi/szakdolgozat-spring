/**
 * Created by zolikapi on 2017. 04. 14..
 */

/**
 * Created by zolikapi on 2017. 04. 14..
 */

var selectedLecture = null;
var allLecture = null;
var allEvent = null;
var allTeacher = null;

$(document).ready(function() {
    moment.locale('hu', {
        week: { dow: 1 }, // Monday is the first day of the week
        format: 'YYYY.MM.DD HH:mm'
    });

    $('input[name="reservation-time"]').daterangepicker(
        {
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 5,
            locale: {
                format: 'YYYY.MM.DD HH:mm'
            },
            startDate: '2019.04.01 00:00',
            endDate: '2019.04.01 00:00',
            weekStart: 6
        },
        function(start, end, label) {
            console.log("A new date range was chosen: " + start.format('YYYY.MM.DD HH:mm') + ' to ' + end.format('YYYY.MM.DD HH:mm'));
        });
    
    NProgress.start();
    $.get('/api/v1/allLecture?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var lectures = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });
                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');

    $.get('/api/v1/allEvent?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var events = new Array();
                response.data.arrayList.forEach(function(event) {
                    events[event.identifier] = event;
                });
                setupSelect(events);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');

    NProgress.start();
    $.get('/api/v1/allTeacher?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var teachers = new Array();
                response.data.arrayList.forEach(function(teacher) {
                    teachers[teacher.identifier] = teacher;
                });
                setupTeacherSelect(teachers);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');

    //only one input is enabled (select or input field)
    $('#teacher-input').bind('input', function() {
        var value = $(this).val(); // get the current value of the input field.
        var select = document.getElementById('teacher-selector');
        select.value = "";
    });

    $('#teacher-selector').on('change', function() {
        var t = document.getElementById('teacher-selector');
        var value = t.options[t.selectedIndex].text;
        var teacherInput = document.getElementById("teacher-input");
        teacherInput.value = "";
    })

});

function setupSelect(events){
    allEvent = events;
    $("#event-selector").empty();
    var select = document.getElementById('event-selector');
    
    for (id in events) {
        var event = events[id];
        
        var opt = document.createElement('option');
        opt.value = id;
        opt.innerHTML = event.name;
        select.appendChild(opt);
    }
}

function setupTeacherSelect(teachers){
    allTeacher = teachers;
    $("#teacher-selector").empty();
    var select = document.getElementById('teacher-selector');

    for (id in teachers) {
        var teacher = teachers[id];

        var opt = document.createElement('option');
        opt.value = teacher.identifier;
        opt.innerHTML = teacher.userName;
        select.appendChild(opt);
    }
}

function reloadData(lectures) {
    allLecture = lectures;

    //delete all rows from table
    var table = $('#datatable').DataTable();
    table
        .clear()
        .draw();

    for (id in lectures) {
        var lecture = lectures[id];
        addRow(lecture);
    }
}

function uploadImage(){
    
    var image = document.getElementById("input-image");
    var file = image.files[0];
    var formData = new FormData();
    formData.append("file", file);
    
    $.ajax({
        url: '/api/v1/uploadFile',
        type: 'POST',

        data: formData,
        
        cache: false,
        contentType: false,
        processData: false,

        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            myXhr.onreadystatechange = function() {
                if (myXhr.readyState == 4 && myXhr.status == 200) {
                    var jsonResponse = JSON.parse(myXhr.responseText);
                    if (jsonResponse.status.status === 200){
                        console.log(myXhr.responseText); // handle response.
                        var imageUrl = jsonResponse.data.string;
                        console.log(imageUrl); // handle response.
                        sendLectureForm(imageUrl);
                    }
                }
            };
            
            return myXhr;
        },
    });
}

function submitNewLecture() {
    
    var image = document.getElementById("input-image");
    if (image.files.length == 0){
        sendLectureForm(null);
    }else{
        //TODO: validation, resize
        uploadImage();
    }
}

function teacherInputChanged(){
    var teacherInput = document.getElementById("teacher-input");
    console.log(teacherInput.value);
}

function sendLectureForm(imageUrl){
    var name = document.getElementById("name-input");
    var info = document.getElementById("info-input");
    var place = document.getElementById("place-input");
    
    var e = document.getElementById("event-selector");
        var selectedEventId = e.options[e.selectedIndex].value;
        var selectedEventName = e.options[e.selectedIndex].text;
    var t = document.getElementById("teacher-selector");
    var teacherInput = document.getElementById("teacher-input");
    var teacherName
    var teacherId;

    if (teacherInput.value != ''){
        teacherId = null;
        teacherName = teacherInput.value;
    } else {
        teacherId = t.options[t.selectedIndex].value;
        teacherName = t.options[t.selectedIndex].text;
    }

    var date = $("#reservation-time").data('daterangepicker');

    var params;
    if (selectedLecture != null){
        params = {loginToken: localStorage.login_token, identifier : selectedLecture.identifier, name : name.value, info : info.value, imageUrl : imageUrl, placeName : place.value, eventId : selectedEventId, eventName : selectedEventName,  startDate : date.startDate.format('X'), endDate : date.endDate.format('X'), teacherId : teacherId,
        teacherName : teacherName};
    }else{
        params = {loginToken: localStorage.login_token, name : name.value, info : info.value, imageUrl : imageUrl, placeName : place.value, eventId : selectedEventId, eventName : selectedEventName,  startDate : date.startDate.format('X'), endDate : date.endDate.format('X'), teacherId : teacherId,
            teacherName : teacherName };
    }

    console.log(params);

    NProgress.start();
    $.post('/api/v1/createOrUpdateLecture', params,
        function(response){
            NProgress.done();
            console.log(response);
            if (response.status.status === 200){

                var lectures = new Array;
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });

                var date = $("#reservation-time").data('daterangepicker');
                var image = document.getElementById("input-image");
                name.value = "";
                info.value = "";
                place.value = "";
                e.value = "";
                t.value = "";
                date.setStartDate(new Date());
                date.setEndDate(new Date());
                image.value = "";
                teacherInput.value = "";

                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
}

function editLecture(id){
    var lecture = allLecture[id];

    if (lecture){
        selectedLecture = lecture;
        //fill the form
        var name = document.getElementById("name-input");
        var info = document.getElementById("info-input");
        var place = document.getElementById("place-input");
        var eventSelector = document.getElementById("event-selector");
        var teacherSelector = document.getElementById("teacher-selector");
        var teacherInput = document.getElementById("teacher-input");

        name.value = lecture.name;
        info.value = lecture.info;
        place.value = lecture.placeName;
        eventSelector.value = lecture.eventId;
        if (lecture.teacherId != null && lecture.teacherId != ""){
            teacherInput.value = "";
            teacherSelector.value = lecture.teacherId;
        } else{
            teacherInput.value = lecture.teacherName;
            teacherSelector.value = "";
        }

        var date = $("#reservation-time").data('daterangepicker');
        date.setStartDate(new Date(lecture.startTimeStamp * 1000));
        date.setEndDate(new Date(lecture.endTimeStamp * 1000));


        var divPosition = $('#lecture-form').offset();
        $('html, body').animate({scrollTop: divPosition.top}, "slow");
    }else{
        alert('Hiba történt!');
    }

}

function deleteLecture(id){    
    var params = {loginToken: localStorage.login_token, identifier : id};

    NProgress.start();
    $.post('/api/v1/deleteLecture', params,
     function(response){
        NProgress.done();
        console.log(response);
        if (response.status.status === 200){

                var lectures = new Array;
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });

                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
    }, 'json');
}

function addRow(lecture){
    var t = $('#datatable').DataTable();
    var edit = "<button type=\"button\" class=\"btn btn-info btn-xs\" href=\"#\" onclick=\"editLecture('" +
        lecture.identifier +
        "');return false;\"><i class=\"fa fa-edit m-right-xs\"></i> Szerkeszt</button>" +
        "<button type=\"button\" class=\"btn btn-danger btn-xs\" data-toggle=\"modal\" data-target=\".bs-delete-modal-lg\" href=\"#\" onclick=\"deleteLecture('" +
        lecture.identifier +
        "');return false;\">Törlés</button>";

    t.row.add( [
        lecture.name,
        lecture.info,
        lecture.eventName,
        lecture.teacherName,
        lecture.placeName,
        timeConverter(lecture.startTimeStamp),
        timeConverter(lecture.endTimeStamp),
        edit
    ] ).draw( false );
}
