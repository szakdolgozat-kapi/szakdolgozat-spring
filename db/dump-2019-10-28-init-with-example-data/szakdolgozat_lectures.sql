-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: szakdolgozat
-- ------------------------------------------------------
-- Server version	5.7.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lectures`
--

DROP TABLE IF EXISTS `lectures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lectures` (
  `identifier` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `info` text,
  `askaquestion_identifier` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `place_name` varchar(255) DEFAULT NULL,
  `event_id` bigint(20) NOT NULL,
  `start_time_stamp` bigint(20) NOT NULL,
  `end_time_stamp` bigint(20) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `teacher_id` bigint(20) DEFAULT NULL,
  `teacher_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`identifier`),
  KEY `FKeoyxv5pjq4bljo7w5oy79qrkj` (`event_id`),
  CONSTRAINT `FKeoyxv5pjq4bljo7w5oy79qrkj` FOREIGN KEY (`event_id`) REFERENCES `events` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lectures`
--

LOCK TABLES `lectures` WRITE;
/*!40000 ALTER TABLE `lectures` DISABLE KEYS */;
INSERT INTO `lectures` VALUES (1,'Lecture 1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','0006','https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',1,1484576801,1484584001,'',0,NULL,NULL),(2,'Lecture 2','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',1,1484584001,1484590000,'',0,NULL,NULL),(3,'Lecture 3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',1,1484590000,1484596000,'',0,NULL,NULL),(4,'Lecture 4','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','0006','https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484596000,1484602000,'',0,NULL,NULL),(5,'Lecture 5','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484602000,1484608000,'',0,NULL,NULL),(6,'Lecture 6','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484608000,1484614000,'',0,NULL,NULL),(7,'Lecture 7','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',2,1484614000,1484620000,'',0,NULL,NULL),(8,'Lecture 8','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg','IK-F01',3,1484922401,1484936801,'',0,NULL,NULL),(9,'Lecture 9','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'','IK-F01',3,1484936741,1484943941,'Test event 3',0,NULL,NULL),(10,'Test name Lecture','Test info',NULL,'127.0.0.1:8080/api/v1/files/1492949573430.png','IK-F02',4,1492336800,1492436740,'Test name mod',0,NULL,NULL),(11,'Teszt előadás','Ez egy teszt előadás',NULL,'127.0.0.1:8080/api/v1/files/1492951604889.png','IK-F01',3,1491638400,1491645600,'Test event 3',0,NULL,NULL),(12,'NEW acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239255,1493269255,'Test name',0,NULL,NULL),(13,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(14,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(15,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(16,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL),(17,'acceptable lecture','info 1',NULL,NULL,'test place 1',4,1493239315,1493269315,'Test name mod',0,NULL,NULL);
/*!40000 ALTER TABLE `lectures` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-28 23:39:10
