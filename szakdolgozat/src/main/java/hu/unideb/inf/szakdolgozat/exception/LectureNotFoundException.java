package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2016. 12. 10..
 */

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such Lecture")  // 404
public class LectureNotFoundException extends USException {
    public LectureNotFoundException() {
        status = HttpStatus.NOT_FOUND.value();
        messageEn = R.status.error.message.lectureNotFound.en;
        messageHu = R.status.error.message.lectureNotFound.hu;
    }
}
