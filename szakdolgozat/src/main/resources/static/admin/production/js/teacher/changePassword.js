/**
 * Created by zolikapi on 2017. 05. 02..
 */

$(document).ready(function() {

});

function changePassword(){
    var email = document.getElementById("email").value;
    var pass0 = document.getElementById("pwd0").value;
    var pass1 = document.getElementById("pwd1").value;

    if (pass0 != pass1){
        alert('A megadott jelszavak nem egyeznek!');
        return;
    }

    params = {email: email, password : pass0};

    $.post('/api/v1/changePassword', params,
        function(response){
            console.log(response);
            if (response.status.status === 200){

                localStorage.setItem("user_icon", response.data.loginResponse.iconUrl);
                localStorage.setItem("user_name", response.data.loginResponse.userName);
                localStorage.setItem("login_token", response.data.loginResponse.loginToken);

                window.location.href = response.data.loginResponse.redirectUrl;

            }else{
                if (response && response.data && response.data.hashMap && response.data.hashMap.redirectUrl){
                    if(typeof response.data.hashMap.redirectUrl !== "undefined"){
                        window.location.href = response.data.hashMap.redirectUrl;
                    }
                }
                alert(response.status.messageHu);
            }
        }, 'json');
}