package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2017. 04. 09..
 */

@ResponseStatus(value= HttpStatus.CONFLICT, reason="User already exists")  // 409
public class UserAlreadyExistsException extends USException {
    public UserAlreadyExistsException() {
        status = HttpStatus.CONFLICT.value();
        messageEn = R.status.error.message.userAlreadyExists.en;
        messageHu = R.status.error.message.userAlreadyExists.hu;
    }
}