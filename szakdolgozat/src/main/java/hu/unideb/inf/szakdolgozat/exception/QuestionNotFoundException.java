package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2016. 12. 23..
 */

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such Question")  // 404
public class QuestionNotFoundException extends USException {
    public QuestionNotFoundException() {
        status = HttpStatus.NOT_FOUND.value();
        messageEn = R.status.error.message.questionNotFound.en;
        messageHu = R.status.error.message.questionNotFound.hu;
    }
}