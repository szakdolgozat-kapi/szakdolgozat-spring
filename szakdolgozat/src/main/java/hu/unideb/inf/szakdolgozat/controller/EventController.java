package hu.unideb.inf.szakdolgozat.controller;

import hu.unideb.inf.szakdolgozat.exception.USException;
import hu.unideb.inf.szakdolgozat.model.persistence.EventPersistence;
import hu.unideb.inf.szakdolgozat.model.response.ResponseDataWrapper;
import hu.unideb.inf.szakdolgozat.model.response.Status;
import hu.unideb.inf.szakdolgozat.model.response.USResponse;
import hu.unideb.inf.szakdolgozat.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static hu.unideb.inf.szakdolgozat.constant.AppConstants.apiv1Prefix;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by zolikapi on 2017. 01. 15..
 */
@RestController
public class EventController {

    @Autowired
    private EventService eventService;

    /*
    * Get all event
    *
    * returns with an array of events (or an empty array)
    *
     */
    @RequestMapping(value= apiv1Prefix + "/events", method=GET)
    public USResponse getEvents() {
        List<EventPersistence> events = eventService.getAllEvent(true);
        ResponseDataWrapper data = new ResponseDataWrapper(events);
        return USResponse.successResponse(data);
    }

    /*
    * Get event by id
    *
    * returns with an array of events (or an empty array)
    *
     */
    @RequestMapping(value= apiv1Prefix + "/event", method=GET)
    @ResponseBody
    public USResponse getEvent(@RequestParam(value="eventId")Integer eventId) {
        EventPersistence event = eventService.getEvent(eventId);
        ResponseDataWrapper data = new ResponseDataWrapper(event);
        return USResponse.successResponse(data);
    }

    /*
     *   Exception handler
     *
     *   @status code: will be specified at custom error class,
     *   otherwise it will be 500 (with localized - en, hu - error message)
     *
     */
    @ExceptionHandler(Exception.class)
    public USResponse handleError(Exception ex) {
        Status status = null;
        if (USException.class.isAssignableFrom(ex.getClass())){
            status = new Status((USException)ex);
        }else{
            status = Status.generalError();
        }

        ResponseDataWrapper data = null;

        if (ex.getMessage() != null){
            data = new ResponseDataWrapper(ex.getMessage());
        } else {
            data = new ResponseDataWrapper(ex.toString());
        }

        USResponse response = new USResponse(status, data);
        return response;
    }

}
