package hu.unideb.inf.szakdolgozat.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;
import hu.unideb.inf.szakdolgozat.util.DateUtil;

/**
 * Created by zolikapi on 2016. 12. 10..
 */

@JsonIgnoreProperties({"teacherUUID"})
public class Lecture {
    private String identifier;
    private String name;
    private String info;
    private String teacherUUID;
    private long created;
    private long endDate;

    public Lecture(String identifier, String teacherUUID) {
        this.identifier = identifier;
        this.teacherUUID = teacherUUID;
        this.created = DateUtil.getUTCTimeStamp();
    }

    public Lecture(String identifier, String name, String info, String teacherUUID) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.teacherUUID = teacherUUID;
        this.created = DateUtil.getUTCTimeStamp();
    }

    public Lecture(String identifier, String name, String info, String teacherUUID, long endDate) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.teacherUUID = teacherUUID;
        this.endDate = endDate;
    }

    public Lecture(String identifier, String name, String info, String teacherUUID, long created, long endDate) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.teacherUUID = teacherUUID;
        this.created = created;
        this.endDate = endDate;
    }

    public void setInfo(Lecture lecture){
        this.name = lecture.name;
        this.info = lecture.info;
        this.endDate = lecture.endDate;
    }

    public void setInfo(LecturePersistence lecture){
        this.name = lecture.getName();
        this.info = lecture.getInfo();
        this.endDate = lecture.getEndTimeStamp();
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public String getTeacherUUID() {
        return teacherUUID;
    }

    public long getCreated() {
        return created;
    }

    public long getEndDate() {
        return endDate;
    }
}
