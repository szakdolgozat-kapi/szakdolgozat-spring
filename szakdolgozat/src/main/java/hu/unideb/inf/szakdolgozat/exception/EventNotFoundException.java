package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2017. 05. 01..
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such Event")  // 404
public class EventNotFoundException extends USException {
    public EventNotFoundException() {
        status = HttpStatus.NOT_FOUND.value();
        messageEn = R.status.error.message.eventNotFound.en;
        messageHu = R.status.error.message.eventNotFound.hu;
    }
}