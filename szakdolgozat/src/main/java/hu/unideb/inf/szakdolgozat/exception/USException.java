package hu.unideb.inf.szakdolgozat.exception;

/**
 * Created by zolikapi on 2017. 01. 15..
 */
public abstract class USException extends RuntimeException {
    public int status;
    public String messageEn;
    public String messageHu;
}