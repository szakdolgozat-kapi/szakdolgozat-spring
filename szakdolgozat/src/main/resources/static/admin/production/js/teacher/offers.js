/**
 * Created by zolikapi on 2017. 04. 27..
 */

$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/allOffer?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var offers = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    offers[lecture.identifier] = lecture;
                });
                reloadData(offers);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');

});

function reloadData(offers) {
    allOffer = offers;

    //delete all rows from table
    var table = $('#datatable').DataTable();
    table
        .clear()
        .draw();

    for (id in offers) {
        var offer = offers[id];
        addRow(offer);
    }
}

function addRow(offer){
    var t = $('#datatable').DataTable();

    t.row.add( [
        offer.teacher,
        offer.course,
        offer.eventName,
        offer.offer
    ] ).draw( false );
}