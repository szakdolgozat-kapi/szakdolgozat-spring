package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2017. 04. 09..
 */

@ResponseStatus(value= HttpStatus.UNAUTHORIZED, reason="Expired token")  // 401
public class ExpiredTokenException extends USException {
    String email;

    public ExpiredTokenException(String email) {
        status = HttpStatus.UNAUTHORIZED.value();
        messageEn = R.status.error.message.expiredToken.en;
        messageHu = R.status.error.message.expiredToken.hu;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}