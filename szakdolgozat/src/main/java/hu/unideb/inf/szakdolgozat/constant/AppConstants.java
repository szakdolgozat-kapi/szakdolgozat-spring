package hu.unideb.inf.szakdolgozat.constant;

/**
 * Created by zolikapi on 2017. 01. 15..
 */
public class AppConstants {
    public final static int maximumNOLecture = 9999;
    public final static long questionLimitTime = 300000; //5 minute in millis
    public final static String apiv1Prefix = "api/v1";
    public final static int cookieValidity = 86400; //24 hours in seconds
    public final static String defaultAdminCompany = "Debreceni Egyetem";

    public final static String noreplayEmail = "unislido@gmail.com";
    public final static String registerEmailSubject = "Regisztrácó";
    public final static String registerAdminBody = "Üdvözöljük az Unislido rendszerében!\n\nÖnt adminisztrátorként regisztrálták a rendszerben. A jelszava beállításához kattintson az alábbi linkre: http://unislido.tk/change_password.\n\nÜdvözlettel: Unislido";
    public final static String registerTeacherBody = "Üdvözöljük az Unislido rendszerében!\n\nÖn előadóként regisztrált a rendszerben.\n\nÜdvözlettel: Unislido";
}
