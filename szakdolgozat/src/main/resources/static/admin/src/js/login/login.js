/**
 * Created by zolikapi on 2017. 04. 09..
 */

/* Adding the script tag to the head as suggested before */

var head = document.getElementsByTagName('head')[0];
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = "http://code.jquery.com/jquery-2.2.1.min.js";

// Then bind the event to the callback function.
// There are several events for cross browser compatibility.
script.onreadystatechange = handler;
script.onload = handler;

// Fire the loading
head.appendChild(script);

function handler(){
    console.log('jquery added :)');
}

function login(){
    var user = document.getElementById("userName").value;
    var pass = document.getElementById("password").value;
    
    $.post('/api/v1/login', { user: user, pass : pass},
     function(returnedData){
        console.log(returnedData);
        if (returnedData.status.status === 200){
            window.location.href = returnedData.data.loginResponse.redirectUrl;
        }else{
            alert("Nem megfelelő felhasználónév/jelszó");
        }
    }, 'json');
}