package hu.unideb.inf.szakdolgozat.web;

/**
 * Created by zolikapi on 2017. 03. 02..
 */

import hu.unideb.inf.szakdolgozat.email.EmailSender;
import hu.unideb.inf.szakdolgozat.exception.ExpiredTokenException;
import hu.unideb.inf.szakdolgozat.exception.GeneralError;
import hu.unideb.inf.szakdolgozat.exception.InvalidEmailException;
import hu.unideb.inf.szakdolgozat.exception.USException;
import hu.unideb.inf.szakdolgozat.model.response.LoginResponse;
import hu.unideb.inf.szakdolgozat.model.response.ResponseDataWrapper;
import hu.unideb.inf.szakdolgozat.model.response.Status;
import hu.unideb.inf.szakdolgozat.model.response.USResponse;
import hu.unideb.inf.szakdolgozat.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.SendFailedException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import static hu.unideb.inf.szakdolgozat.constant.AppConstants.apiv1Prefix;
import static hu.unideb.inf.szakdolgozat.constant.AppConstants.registerAdminBody;
import static hu.unideb.inf.szakdolgozat.constant.AppConstants.registerEmailSubject;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private EmailSender emailSender;

    @CrossOrigin(allowCredentials = "false")
    @RequestMapping(value= apiv1Prefix + "/login", method=POST)
    public USResponse login(@RequestParam(value="email") String email,
                            @RequestParam(value="pass") String pass) {

        LoginResponse loginResponse = loginService.login(email, pass);

        loginResponse.loginToken = loginService.createLoginToken(email);

        ResponseDataWrapper data = new ResponseDataWrapper(loginResponse);
        return USResponse.successResponse(data);
    }

    @CrossOrigin(allowCredentials = "false")
    @RequestMapping(value= apiv1Prefix + "/logout", method=POST)
    public USResponse logout(@RequestParam(value="loginToken") String token) {

        ResponseDataWrapper data = new ResponseDataWrapper(loginService.deleteLoginToken(token));
        return USResponse.successResponse(data);
    }

    @CrossOrigin
    @RequestMapping(value= apiv1Prefix + "/register", method=POST)
    public USResponse register(@RequestParam(value="name") String name,
                               @RequestParam(value="email") String email,
                               @RequestParam(value="phone") String phone,
                               @RequestParam(value="pass") String pass,
                               @RequestParam(value="company") String company,
                               @RequestParam(value="imageUrl") String imageUrl) {
        LoginResponse loginResponse = loginService.register(name, email, phone, pass, company, imageUrl);

        loginResponse.loginToken = loginService.createLoginToken(email);

        try {
            emailSender.sendEmail(email, registerEmailSubject, registerAdminBody);
        }catch (SendFailedException exception){
            //delete the user and token is already created
            try {
                loginService.deleteUser(email);
            }catch (GeneralError error){
                //there is no saved user yet
            }

            if (loginResponse.loginToken != null){
                loginService.deleteLoginToken(loginResponse.loginToken);
            }

            throw new InvalidEmailException();
        }


        ResponseDataWrapper data = new ResponseDataWrapper(loginResponse);
        return USResponse.successResponse(data);
    }


    @ExceptionHandler(Exception.class)
    public USResponse handleError(Exception ex) {
        ex.printStackTrace();
        Status status = null;
        if (USException.class.isAssignableFrom(ex.getClass())){
            status = new Status((USException)ex);
        }else{
            status = Status.generalError();
        }

        ResponseDataWrapper data = null;

        if (ex.getMessage() != null){
            data = new ResponseDataWrapper(ex.getMessage());
        } else {
            data = new ResponseDataWrapper(ex.toString());
        }

        USResponse response = new USResponse(status, data);
        return response;
    }
}
