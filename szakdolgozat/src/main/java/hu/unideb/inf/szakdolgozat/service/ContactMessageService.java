package hu.unideb.inf.szakdolgozat.service;

import hu.unideb.inf.szakdolgozat.model.persistence.ContactMessagePersistence;
import hu.unideb.inf.szakdolgozat.repository.ContactMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zolikapi on 2017. 04. 30..
 */

@Service
public class ContactMessageService {
    @Autowired
    private ContactMessageRepository contactMessageRepository;

    public Boolean saveMessage(ContactMessagePersistence contactMessagePersistence){
        contactMessageRepository.save(contactMessagePersistence);
        return true;
    }

    public List<ContactMessagePersistence> getAllContactMessage(){
        return contactMessageRepository.findAll();
    }

}
