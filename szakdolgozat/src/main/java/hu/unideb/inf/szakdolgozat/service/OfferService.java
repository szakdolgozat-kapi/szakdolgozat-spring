package hu.unideb.inf.szakdolgozat.service;

import hu.unideb.inf.szakdolgozat.exception.GeneralError;
import hu.unideb.inf.szakdolgozat.model.persistence.OfferPersistence;
import hu.unideb.inf.szakdolgozat.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zolikapi on 2017. 04. 16..
 */
@Service
public class OfferService {

    @Autowired
    private OfferRepository offerRepository;

    public List<OfferPersistence> getAllOffer(){
        return offerRepository.findAll();
    }

    public Boolean createOrUpdateOffer(OfferPersistence offer){
        offerRepository.save(offer);
        return true;
    }

    public Boolean delete(long identifier){
        List<OfferPersistence> list = offerRepository.findByIdentifier(identifier);
        if (list != null && list.size() == 1){
            OfferPersistence o = list.get(0);
            o.setDeleted(true);
            offerRepository.save(o);
            return true;
        } else {
            throw new GeneralError();
        }
    }
}
