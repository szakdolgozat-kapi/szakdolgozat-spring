/**
 * Created by zolikapi on 2017. 04. 14..
 */

var allLecture = null;
var allEvent = null;

$(document).ready(function() {
    moment.locale('hu', {
        week: { dow: 1 }, // Monday is the first day of the week
        format: 'YYYY.MM.DD HH:mm'
    });

    $('input[name="reservation-time"]').daterangepicker(
        {
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 5,
            locale: {
                format: 'YYYY.MM.DD HH:mm'
            },
            startDate: '2019.04.01 00:00',
            endDate: '2019.04.01 00:00',
            weekStart: 6
        },
        function(start, end, label) {
            console.log("A new date range was chosen: " + start.format('YYYY.MM.DD HH:mm') + ' to ' + end.format('YYYY.MM.DD HH:mm'));
        });

    NProgress.start();
    $.get('/api/v1/allLecture?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var lectures = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });
                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');

    $.get('/api/v1/allEvent?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var events = new Array();
                response.data.arrayList.forEach(function(event) {
                    events[event.identifier] = event;
                });
                setupSelect(events);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});

function setupSelect(events){
    allEvent = events;
    $("#event-selector").empty();
    var select = document.getElementById('event-selector');

    for (id in events) {
        var event = events[id];

        var opt = document.createElement('option');
        opt.value = id;
        opt.innerHTML = event.name;
        select.appendChild(opt);
    }
}

function reloadData(lectures) {
    allLecture = lectures;

    //delete all rows from table
    var table = $('#datatable').DataTable();
    table
        .clear()
        .draw();

    for (id in lectures) {
        var lecture = lectures[id];
        addRow(lecture);
    }
}

function uploadImage(){

    var image = document.getElementById("input-image");
    var file = image.files[0];
    var formData = new FormData();
    formData.append("file", file);

    $.ajax({
        url: '/api/v1/uploadFile',
        type: 'POST',

        data: formData,

        cache: false,
        contentType: false,
        processData: false,

        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            myXhr.onreadystatechange = function() {
                if (myXhr.readyState == 4 && myXhr.status == 200) {
                    var jsonResponse = JSON.parse(myXhr.responseText);
                    if (jsonResponse.status.status === 200){
                        console.log(myXhr.responseText); // handle response.
                        var imageUrl = jsonResponse.data.string;
                        //TODO: send submit data with this url
                        console.log(imageUrl); // handle response.
                        sendLectureForm(imageUrl);
                    }
                }
            };

            return myXhr;
        },
    });
}

function submitNewLecture() {

    var image = document.getElementById("input-image");
    if (image.files.length == 0){
        sendLectureForm(null);
    }else{
        //TODO: validation, resize
        uploadImage();
    }
}

function sendLectureForm(imageUrl){
    var name = document.getElementById("name-input");
    var info = document.getElementById("info-input");
    var place = document.getElementById("place-input");

    var e = document.getElementById("event-selector");
    var selectedEventId = e.options[e.selectedIndex].value;
    var selectedEventName = e.options[e.selectedIndex].text;
    var date = $("#reservation-time").data('daterangepicker');

    var params = {loginToken: localStorage.login_token, name : name.value, info : info.value, imageUrl : imageUrl, placeName : place.value, eventId : selectedEventId, eventName : selectedEventName,  startDate : date.startDate.format('X'), endDate : date.endDate.format('X') };

    NProgress.start();
    $.post('/api/v1/registerOwnLecture', params,
        function(response){
            NProgress.done();
            console.log(response);
            if (response.status.status === 200){

                var lectures = new Array;
                response.data.arrayList.forEach(function(lecture) {
                    lectures[lecture.identifier] = lecture;
                });

                var name = document.getElementById("name-input");
                var info = document.getElementById("info-input");
                var place = document.getElementById("place-input");
                var e = document.getElementById("event-selector");
                var date = $("#reservation-time").data('daterangepicker');
                var image = document.getElementById("input-image");
                name.value = "";
                info.value = "";
                place.value = "";
                e.value = "";
                date.setStartDate(new Date());
                date.setEndDate(new Date());
                image.value = "";

                reloadData(lectures);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
}

function addRow(lecture){
    var t = $('#datatable').DataTable();

    t.row.add( [
        lecture.name,
        lecture.info,
        lecture.eventName,
        lecture.teacherName,
        lecture.placeName,
        timeConverter(lecture.startTimeStamp),
        timeConverter(lecture.endTimeStamp)
    ] ).draw( false );
}
