package hu.unideb.inf.szakdolgozat.model.response;

/**
 * Created by zolikapi on 2017. 04. 09..
 */
public class LoginResponse {
    public String redirectUrl;
    public Authority authority;
    public String iconUrl;
    public String userName;
    public String loginToken;

    public enum Authority{
        USER, ADMIN, SUPERADMIN
    }

    public LoginResponse() {
    }

    public LoginResponse(String redirectUrl, Authority authority, String iconUrl, String userName) {
        this.redirectUrl = redirectUrl;
        this.authority = authority;
        this.iconUrl = iconUrl;
        this.userName = userName;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public Authority getAuthority() {
        return authority;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
