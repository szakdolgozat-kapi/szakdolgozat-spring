/**
 * Created by zolikapi on 2017. 04. 27..
 */

var selectedOffer = null;
var allOffer = null;
var allEvent = null;
var deletableOfferId = null;

$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/allOffer?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var offers = new Array();
                response.data.arrayList.forEach(function(lecture) {
                    offers[lecture.identifier] = lecture;
                });
                reloadData(offers);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');

    $.get('/api/v1/allEvent?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var events = new Array();
                response.data.arrayList.forEach(function(event) {
                    events[event.identifier] = event;
                });
                setupSelect(events);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});

function setupSelect(events){
    allEvent = events;
    $("#event-selector").empty();
    var select = document.getElementById('event-selector');

    for (id in events) {
        var event = events[id];

        var opt = document.createElement('option');
        opt.value = id;
        opt.innerHTML = event.name;
        select.appendChild(opt);
    }
}

function reloadData(offers) {
    allOffer = offers;

    //delete all rows from table
    var table = $('#datatable').DataTable();
    table
        .clear()
        .draw();

    for (id in offers) {
        var offer = offers[id];
        addRow(offer);
    }
}

function submitNewOffer() {

    console.log('submitNewOffer');

    var teacher = document.getElementById("teacher-input");
    var course = document.getElementById("course-input");
    var e = document.getElementById("event-selector");
        var selectedEventId = e.options[e.selectedIndex].value;
        var selectedEventName = e.options[e.selectedIndex].text;
    var offer = document.getElementById("offer-textarea");

    var params;
    if (selectedOffer != null){
        params = {loginToken: localStorage.login_token, identifier : selectedOffer.identifier, teacher : teacher.value, course : course.value, offer : offer.value, eventId : selectedEventId, eventName : selectedEventName };
    }else{
        params = {loginToken: localStorage.login_token, teacher : teacher.value, course : course.value, offer : offer.value, eventId : selectedEventId, eventName : selectedEventName };
    }

    console.log(params);

    NProgress.start();
    $.post('/api/v1/createOrUpdateOffer', params,
        function(response){
            NProgress.done();
            console.log(response);
            if (response.status.status === 200){

                var offers = new Array;
                response.data.arrayList.forEach(function(offer) {
                    offers[offer.identifier] = offer;
                });


                var teacher = document.getElementById("teacher-input");
                var course = document.getElementById("course-input");
                var e = document.getElementById("event-selector");
                var offer = document.getElementById("offer-textarea");

                teacher.value = "";
                course.value = "";
                offer.value = "";
                e.value = "";

                reloadData(offers);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
}

function editOffer(id){
    console.log('editOffer: '+ id);

    var offer = allOffer[id];

    if (offer){
        selectedOffer = offer;
        //fill the form
        var teacher = document.getElementById("teacher-input");
        var course = document.getElementById("course-input");
        var eventSelector = document.getElementById("event-selector");
        var o = document.getElementById("offer-textarea");

        teacher.value = offer.teacher;
        course.value = offer.course;
        o.value = offer.offer;
        eventSelector.value = offer.eventId;

        var divPosition = $('#fuckyou-form').offset();
        $('html, body').animate({scrollTop: divPosition.top}, "slow");
    }else{
        alert('Hiba történt!');
    }

}

function deleteOffer(id){
    deletableOfferId = id;
}

function sendDeletableOffer(){
    var params = {loginToken: localStorage.login_token, identifier : deletableOfferId};

    NProgress.start();
    $.post('/api/v1/deleteOffer', params,
        function(response){
            NProgress.done();
            console.log(response);
            if (response.status.status === 200){

                var offers = new Array;
                response.data.arrayList.forEach(function(offer) {
                    offers[offer.identifier] = offer;
                });

                var teacher = document.getElementById("teacher-input");
                var course = document.getElementById("course-input");
                var offer = document.getElementById("offer-textarea");
                teacher.value = "";
                course.value = "";
                offer.value = "";

                reloadData(offers);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');
}

function addRow(offer){
    var t = $('#datatable').DataTable();
    var edit = "<button type=\"button\" class=\"btn btn-info btn-xs\" href=\"#\" onclick=\"editOffer('" +
        offer.identifier +
        "');return false;\"><i class=\"fa fa-edit m-right-xs\"></i> Szerkeszt</button>" +
        "<button type=\"button\" class=\"btn btn-danger btn-xs\" data-toggle=\"modal\" data-target=\".bs-delete-modal-lg\" href=\"#\" onclick=\"deleteOffer('" +
        offer.identifier +
        "');return false;\">Törlés</button>";

    t.row.add( [
        offer.teacher,
        offer.course,
        offer.eventName,
        offer.offer,
        edit
    ] ).draw( false );
}