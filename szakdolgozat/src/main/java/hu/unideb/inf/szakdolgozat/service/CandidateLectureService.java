package hu.unideb.inf.szakdolgozat.service;

/**
 * Created by zolikapi on 2017. 04. 26..
 */

import hu.unideb.inf.szakdolgozat.email.EmailSender;
import hu.unideb.inf.szakdolgozat.exception.GeneralError;
import hu.unideb.inf.szakdolgozat.model.persistence.CandidateLecturePersistence;
import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;
import hu.unideb.inf.szakdolgozat.repository.CandidateLectureRepository;
import hu.unideb.inf.szakdolgozat.repository.LectureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CandidateLectureService {

    @Autowired
    private CandidateLectureRepository candidateLectureRepository;
    @Autowired
    private LectureRepository lectureRepository;

    public List<CandidateLecturePersistence> getAllCandidateLecture(){
        return candidateLectureRepository.findAll();
    }

    public List<CandidateLecturePersistence> getAllWaitingCandidateLecture(){
        return getAllCandidateLecture().stream().filter(l -> l.getStatus() == 0).collect(Collectors.toList());
    }

    public Boolean acceptCandidateLectures(Long[] idList, String token) {
        return modifyCandidateLectures(idList, 1, token);
    }

    public Boolean declineCandidateLectures(Long[] idList) {
        return modifyCandidateLectures(idList, 2, null);
    }

    //status -- 0: waiting, 1: accepted, 2: declined
    private Boolean modifyCandidateLectures(Long[] idList, int status, String token) {
        if (idList == null || idList.length == 0){
            throw new GeneralError();
        }
        List<CandidateLecturePersistence> candidateLectureList = candidateLectureRepository.findByIdentifierIn(new ArrayList<Long>(Arrays.asList(idList)));
        List<LecturePersistence > lectureList = new ArrayList<>();
        if (candidateLectureList != null && candidateLectureList.size() == idList.length){
            for (CandidateLecturePersistence candidateLecture : candidateLectureList) {
                //save the lecture if the admin accept it
                if (status == 1){
                    LecturePersistence lecture = new LecturePersistence(candidateLecture);
                    lectureList.add(lecture);
                }

                //delete the candidate lecture
                candidateLecture.setStatus(status);
            }
            //save the changes
            candidateLectureRepository.save(candidateLectureList);
            if (status == 1) {
                lectureRepository.save(lectureList);
            }
        }
        return true;
    }
}
