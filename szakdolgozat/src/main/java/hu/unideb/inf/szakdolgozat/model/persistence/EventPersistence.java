package hu.unideb.inf.szakdolgozat.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by zolikapi on 2017. 01. 15..
 */




@Entity
@Table(name = "events")
@Where(clause = "deleted=0")
@JsonIgnoreProperties({"deleted"})
public class EventPersistence implements Comparable<EventPersistence>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdentifier() {
        return identifier;
    }

    @OneToMany(mappedBy = "eventId", cascade = CascadeType.ALL)
    @OrderBy("startTimeStamp ASC")
    public Set<LecturePersistence> getLectures() {
        return lectures;
    }



    private long identifier;
    private String name;
    private String info;
    private long startTimeStamp;
    private long endTimeStamp;
    private Set<LecturePersistence> lectures;
    private Set<OfferPersistence> offers;
    private Boolean deleted;

    public EventPersistence() {
        this.deleted = false;
    }

    public EventPersistence(long identifier, String name, String info, long startTimeStamp, long endTimeStamp) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.deleted = false;
    }

    public EventPersistence(String name, String info, long startTimeStamp, long endTimeStamp) {
        this.name = name;
        this.info = info;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.deleted = false;
    }

    public EventPersistence(String name, String info, long startTimeStamp, long endTimeStamp, Set<LecturePersistence> lectures) {
        this.name = name;
        this.info = info;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.lectures = lectures;
        this.deleted = false;
    }

    public EventPersistence(long identifier, String name, String info, long startTimeStamp, long endTimeStamp, Set<LecturePersistence> lectures, Set<OfferPersistence> offers, Boolean deleted) {
        this.identifier = identifier;
        this.name = name;
        this.info = info;
        this.startTimeStamp = startTimeStamp;
        this.endTimeStamp = endTimeStamp;
        this.lectures = lectures;
        this.offers = offers;
        this.deleted = deleted;
    }


    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public long getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setStartTimeStamp(long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public void setEndTimeStamp(long endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }



    public void setLectures(Set<LecturePersistence> lectures) {
        this.lectures = lectures;
    }

    @OneToMany(mappedBy = "eventId", cascade = CascadeType.ALL)
    public Set<OfferPersistence> getOffers() {
        return offers;
    }

    public void setOffers(Set<OfferPersistence> offers) {
        this.offers = offers;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "EventPersistence{" +
                "identifier=" + identifier +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", startTimeStamp=" + startTimeStamp +
                ", endTimeStamp=" + endTimeStamp +
                ", lectures=" + lectures +
                ", offers=" + offers +
                '}';
    }

    @Override
    public int compareTo(EventPersistence o) {
        long x = this.getStartTimeStamp();
        long y = o.getStartTimeStamp();
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
}
