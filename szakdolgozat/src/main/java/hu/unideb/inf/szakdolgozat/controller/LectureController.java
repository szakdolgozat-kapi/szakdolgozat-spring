package hu.unideb.inf.szakdolgozat.controller;

import hu.unideb.inf.szakdolgozat.exception.USException;
import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;
import hu.unideb.inf.szakdolgozat.model.response.ResponseDataWrapper;
import hu.unideb.inf.szakdolgozat.model.response.Status;
import hu.unideb.inf.szakdolgozat.model.response.USResponse;
import hu.unideb.inf.szakdolgozat.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import static hu.unideb.inf.szakdolgozat.constant.AppConstants.apiv1Prefix;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by zolikapi on 2017. 01. 15..
 */


@RestController
public class LectureController {

    @Autowired
    private LectureService lectureService;

    /*
    * Register lecture
    *
    * returns with a unique identifier
    *
     */
    @RequestMapping(value= apiv1Prefix + "/lecture/register", method=POST)
    public USResponse getID(@RequestHeader("UUID") String uuid,
                            @RequestParam(value="identifier") long identifier){
        LecturePersistence lecturePersistence = lectureService.getPersistedLectureByIdentifier(identifier);
        lectureService.bookNextLectureId(uuid, lecturePersistence);
        ResponseDataWrapper data = new ResponseDataWrapper(lecturePersistence);
        return USResponse.successResponse(data);
    }

    /*
    * Get lecture info
    *
    * Retuns the selected lecture's info
    *
    * @RequestParam: lectureId - identifier of Lecture
    */
    @RequestMapping(value= apiv1Prefix + "/lecture", method=GET)
    public USResponse getLecture(@RequestParam(value="lectureId") String id) {
        LecturePersistence lecturePersistence = lectureService.getPersistedLectureByAskAQuestionIdentifier(id);
        ResponseDataWrapper data = new ResponseDataWrapper(lecturePersistence);
        return USResponse.successResponse(data);
    }

    /*
    * Ask a question
    *
    * Any user can ask a new question for a specific lecture
    *
    * @HeaderParam: UUID - uniq user identifier
    *
    * @RequestParam: lectureId - identifier of Lecture
    * @RequestParam: question - text of question
    */
    @RequestMapping(value= apiv1Prefix + "/lecture/can_i_ask", method=GET)
    public USResponse canIAsk(@RequestHeader("UUID") String uuid,
                                   @RequestParam(value="lectureId") String lectureId){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.canIAsk(uuid, lectureId));
        LecturePersistence lecturePersistence = lectureService.getPersistedLectureByAskAQuestionIdentifier(lectureId);
        if (lecturePersistence != null){
            data.addObject(lecturePersistence);
        }
        return USResponse.successResponse(data);
    }

    /*
    * Ask a question
    *
    * Any user can ask a new question for a specific lecture
    *
    * @HeaderParam: UUID - uniq user identifier
    *
    * @RequestParam: lectureId - identifier of Lecture
    * @RequestParam: question - text of question
    */
    @RequestMapping(value= apiv1Prefix + "/lecture/ask", method=POST)
    public USResponse askAQuestion(@RequestHeader("UUID") String uuid,
                                   @RequestParam(value="lectureId") String lectureId,
                                   @RequestParam(value="question") String question){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.askAQuestion(uuid, lectureId, question));
        return USResponse.successResponse(data);
    }

    /*
    * Get the questions for a specific lecture
    *
    * returns the questions this lecture (if the request sender is the lecture's owner)
    *
    * @HeaderParam: UUID - uniq user identifier
    *
    * @RequestParam: lectureId - identifier of Lecture
    */
    @RequestMapping(value= apiv1Prefix + "/lecture/questions", method=GET)
    public USResponse getQuestions(@RequestHeader("UUID") String uuid,
                                   @RequestParam(value="lectureId") String lectureId){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getQuestions(uuid, lectureId));
        return USResponse.successResponse(data);
    }

    /*
    * Get the next questions for a specific lecture
    *
    * returns the nexr questions this lecture (if the request sender is the lecture's owner)
    *
    * @HeaderParam: UUID - uniq user identifier
    *
    * @RequestParam: lectureId - identifier of Lecture
    * @RequestParam: lastQuestionId - identifier of Last question
    */
    @RequestMapping(value= apiv1Prefix + "/lecture/new_questions", method=GET)
    public USResponse getNewQuestions(@RequestHeader("UUID") String uuid,
                                      @RequestParam(value="lectureId") String lectureId,
                                      @RequestParam(value="lastQuestionId") String lastQuestionId) {
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getNewQuestions(uuid, lectureId, lastQuestionId));
        return USResponse.successResponse(data);
    }

    /*
    * Accept a question
    *
    * returns sucess if the request sender is the lecture's owner and there is a question with this id
    *
    * @HeaderParam: UUID - uniq user identifier
    *
    * @RequestParam: lectureId - identifier of Lecture
    * @RequestParam: questionId - identifier of selected Question
    *
    */
    @RequestMapping(value= apiv1Prefix + "/lecture/accept_question", method=POST)
    public USResponse acceptQuestion(@RequestHeader("UUID") String uuid,
                                     @RequestParam(value="lectureId") String lectureId,
                                     @RequestParam(value="questionId") String questionId){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.acceptAQuestion(uuid, lectureId, questionId));
        return USResponse.successResponse(data);
    }

    /*
    * Decline a question
    *
    * returns sucess if the request sender is the lecture's owner and there is a question with this id
    *
    * @HeaderParam: UUID - uniq user identifier
    *
    * @RequestParam: lectureId - identifier of Lecture
    * @RequestParam: questionId - identifier of selected Question
    *
    */
    @RequestMapping(value= apiv1Prefix + "/lecture/decline_question", method=POST)
    public USResponse declineQuestion(@RequestHeader("UUID") String uuid,
                                      @RequestParam(value="lectureId") String lectureId,
                                      @RequestParam(value="questionId") String questionId){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.declineAQuestion(uuid, lectureId, questionId));
        return USResponse.successResponse(data);
    }

    /*
    * Finish the lecture
    *
    * returns sucess if the request sender is the lecture's owner
    *
    * @HeaderParam: UUID - uniq user identifier
    *
    * @RequestParam: lectureId - identifier of Lecture
    *
    */
    @RequestMapping(value= apiv1Prefix + "/lecture/finish", method=POST)
    public USResponse finishLecture(@RequestHeader("UUID") String uuid,
                                    @RequestParam(value="lectureId") String lectureId){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.finishLecture(uuid, lectureId));
        return USResponse.successResponse(data);
    }

    /*
    * Get the accepted question for the specific lecture
    *
    * returns the accepted question for the specific lecture
    *
    * @RequestParam: lectureId - identifier of Lecture
    *
    */
    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value= apiv1Prefix + "/lecture/actual", method=GET)
    public USResponse finishLecture(@RequestParam(value="lectureId") String lectureId){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getAcceptedQuestion(lectureId));
        return USResponse.successResponse(data);
    }

    /*
    * Get the accepted questions for a specific lecture
    *
    * returns the questions this lecture
    *
    * @RequestParam: lectureId - identifier of Lecture
    */
    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping(value= apiv1Prefix + "/lecture/allAcceptedQuestion", method=GET)
    public USResponse getQuestions(@RequestParam(value="lectureId") String lectureId){
        ResponseDataWrapper data = new ResponseDataWrapper(lectureService.getAllAcceptedQuestion(lectureId));
        return USResponse.successResponse(data);
    }

    /*
     *   Exception handler
     *
     *   @status code: will be specified at custom error class,
     *   otherwise it will be 500 (with localized - en, hu - error message)
     *
     */
    @ExceptionHandler(Exception.class)
    public USResponse handleError(Exception ex) {
        Status status = null;
        if (USException.class.isAssignableFrom(ex.getClass())){
            status = new Status((USException)ex);
        }else{
            status = Status.generalError();
        }

        ResponseDataWrapper data = null;

        if (ex.getMessage() != null){
            data = new ResponseDataWrapper(ex.getMessage());
        } else {
            data = new ResponseDataWrapper(ex.toString());
        }

        USResponse response = new USResponse(status, data);
        return response;
    }
}