/**
 * Created by zolikapi on 2017. 04. 14..
 */

var allUser = null;

$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/allTeacher?loginToken=' + localStorage.login_token,
        function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);

                var users = new Array();
                response.data.arrayList.forEach(function(user) {
                    users[user.identifier] = user;
                });
                reloadData(users);

            }else{
                apiErrorHandler(response);
            }
        }, 'json');

});

function reloadData(users) {
    // delete all element from container
    $('#user-container').empty();

    var container = document.getElementById("user-container");

    allUser = users;

    for (id in users) {
        var user = users[id];
        addUser(user, container);
    }
}

function addUser(user, container) {
    var tmp = document.getElementById("profile-template");
    var clone = tmp.cloneNode(true);

    clone.classList.remove("hidden-template");

    var authority = clone.getElementsByClassName("authority")[0];
    var name = clone.getElementsByClassName("name")[0];
    var company = clone.getElementsByClassName("company")[0];
    var email = clone.getElementsByClassName("email")[0];
    var phone = clone.getElementsByClassName("phone")[0];
    var image = clone.getElementsByClassName("profile-image")[0];
    console.log(image);

    if (user.authority === "USER"){
        authority.innerHTML = "Előadó";
    } else if (user.authority === "SUPERADMIN"){
        authority.innerHTML = "Szuperadmin";
    } else if (user.authority === "ADMIN"){
        authority.innerHTML = "Adminisztrátor";
    }

    name.innerHTML = user.userName;
    company.innerHTML = user.company;
    email.innerHTML = "<a href='mailto:" + user.email + "'>" + user.email +"<a>";

    if (typeof user.phone != 'undefined'){
        phone.innerHTML = user.phone;
    }

    if (user.icon === null || user.icon === ""){
        image.src = "images/user.png";
    }else{
        image.src = user.icon;
    }

    container.appendChild(clone);
}