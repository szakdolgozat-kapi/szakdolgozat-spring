package hu.unideb.inf.szakdolgozat.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Created by zolikapi on 2017. 01. 15..
 */

@Entity
@Table(name = "offers")
@Where(clause = "deleted=0")
@JsonIgnoreProperties({"deleted"})
public class OfferPersistence {
    private long identifier;
    private String teacher;
    private String course;
    private String offer;
    private long eventId;
    private String eventName;
    private Boolean deleted;

    public OfferPersistence() {
        this.deleted = false;
    }

    public OfferPersistence(String teacher, String course, String offer) {
        this.teacher = teacher;
        this.course = course;
        this.offer = offer;
        this.deleted = false;
    }

    public OfferPersistence(String teacher, String course, String offer, long eventId, String eventName) {
        this.teacher = teacher;
        this.course = course;
        this.offer = offer;
        this.eventId = eventId;
        this.eventName = eventName;
        this.deleted = false;
    }

    public OfferPersistence(long identifier, String teacher, String course, String offer, long eventId, String eventName, Boolean deleted) {
        this.identifier = identifier;
        this.teacher = teacher;
        this.course = course;
        this.offer = offer;
        this.eventId = eventId;
        this.eventName = eventName;
        this.deleted = deleted;
    }

    public OfferPersistence(long identifier, String teacher, String course, String offer, long eventId, String eventName) {
        this.identifier = identifier;
        this.teacher = teacher;
        this.course = course;
        this.offer = offer;
        this.eventId = eventId;
        this.eventName = eventName;
        this.deleted = false;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdentifier() {
        return identifier;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getCourse() {
        return course;
    }

    public String getOffer() {
        return offer;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public String toString() {
        return "OfferPersistence{" +
                "identifier=" + identifier +
                ", teacher='" + teacher + '\'' +
                ", course='" + course + '\'' +
                ", offer='" + offer + '\'' +
                ", eventId=" + eventId +
                ", eventName='" + eventName + '\'' +
                ", deleted=" + deleted +
                '}';
    }
}
