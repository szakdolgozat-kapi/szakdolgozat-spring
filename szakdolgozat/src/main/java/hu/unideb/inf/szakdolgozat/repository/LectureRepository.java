package hu.unideb.inf.szakdolgozat.repository;

import hu.unideb.inf.szakdolgozat.model.persistence.LecturePersistence;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zolikapi on 2017. 01. 15..
 */



public interface LectureRepository extends JpaRepository<LecturePersistence, Integer> {
    List<LecturePersistence> findByIdentifier(long identifier);
    List<LecturePersistence> findByAskAQuestionIdentifier(String askAQuestionIdentifier);
    List<LecturePersistence> findByTeacherId(long teacherId);
}
