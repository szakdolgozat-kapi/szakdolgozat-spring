/**
 * Created by zolikapi on 2017. 04. 14..
 */

$(document).ready(function() {

    NProgress.start();
    $.get('/api/v1/allEvent?loginToken=' + localStorage.login_token,
         function(response){
            NProgress.done();
            if (response.status.status === 200){
                console.log(response);
                
                var events = new Array();
                response.data.arrayList.forEach(function(event) {
                    events[event.identifier] = event;
                });
                reloadData(events);
                
            }else{
                apiErrorHandler(response);
            }
        }, 'json');
});

function reloadData(events) {

    //delete all rows from table
    var table = $('#datatable').DataTable();
    table
        .clear()
        .draw();

    for (id in events) {
        var event = events[id];
        addRow(event);
    }
}

function addRow(event){
    var t = $('#datatable').DataTable();

    t.row.add( [
        event.name,
        event.info,
        timeConverter(event.startTimeStamp),
        timeConverter(event.endTimeStamp)
    ] ).draw( false );

}

