package hu.unideb.inf.szakdolgozat.model.persistence;

/**
 * Created by zolikapi on 2017. 04. 30..
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "contactMessages")
@Where(clause = "deleted=0")
@JsonIgnoreProperties({"deleted"})
public class ContactMessagePersistence {
    private long identifier;
    private String name;
    private String email;
    private String phone;
    private String message;
    private Boolean deleted;

    public ContactMessagePersistence() {
        deleted = false;
    }

    public ContactMessagePersistence(long identifier, String name, String email, String phone, String message, Boolean deleted) {
        this.identifier = identifier;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.message = message;
        this.deleted = deleted;
    }

    public ContactMessagePersistence(String name, String email, String phone, String message) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.message = message;
        this.deleted = false;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdentifier() {
        return identifier;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
