package hu.unideb.inf.szakdolgozat.exception;

import hu.unideb.inf.szakdolgozat.constant.R;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by zolikapi on 2017. 04. 19..
 */


@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Forbidden")  // 403
public class PermissionDenied extends USException {
    public PermissionDenied(){
        status = HttpStatus.FORBIDDEN.value();
        messageEn = R.status.error.message.general.en;
        messageHu = R.status.error.message.general.hu;
    }
}
